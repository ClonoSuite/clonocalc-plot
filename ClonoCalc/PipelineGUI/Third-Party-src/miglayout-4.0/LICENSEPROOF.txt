There exists only a BSD-License in offical source folder (http://www.migcalendar.com/miglayout/versions/4.0/ (accessed Mar 07, 2016))
But on the main page (http://www.miglayout.com/ (accessed Mar 07, 2016)) you can read: 
"MigLayout is free to use for commercial and non-commercial projects and the source code is provided. It is licensed under the very free BSD or GPL license, whichever you prefer."

A discussion thread concerning the license model:
http://migcalendar.com/forums/viewtopic.php?f=8&t=2479 (accessed Mar 07, 2016)
