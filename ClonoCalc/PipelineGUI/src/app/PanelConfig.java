package app;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import Listener.CheckBoxAutoCompleteListener;
import Listener.StartButtonListener;
import app.OSDetection.OperatingSys;
import net.miginfocom.swing.MigLayout;

public class PanelConfig implements ActionListener {

	MainFrame mainFrame = null;
	JFrame outerFrame = null;

	final GlobalVars gv = GlobalVars.singleton();

	JPanel panelConfig = new JPanel();
	JPanel panelConfigSamples = new JPanel();
	JPanel panelConfigOptions = new JPanel();
	JPanel panelAdvancedOptions = new JPanel();

	final String fileTypeReadUnpacked = "fastq";
	final String fileTypeReadPacked = "gz";

	JLabel lblForwardRead = new JLabel("Forward read");
	JLabel lblReverseRead = new JLabel("Reverse read");
	JLabel lblOutputFolder = new JLabel("Output folder");

	JButton btnChooseForwardReadFile = new JButton("Browse");
	JButton btnChooseReverseReadFile = new JButton("Browse");
	JButton btnChooseOutputFolder = new JButton("Browse");

	JSeparator separatortop = new JSeparator();
	JSeparator separatortop2 = new JSeparator();
	JSeparator separatortop3 = new JSeparator();

	JSeparator separatorSample = new JSeparator();

	JButton btnAddSampleInputField = new JButton("+");
	JButton btnRemoveSampleInputField = new JButton("-");

	JButton btnRestoreDefaults = new JButton("Restore defaults");

	JCheckBox cbAutoComplete = new JCheckBox(gv.cbAutoCompleteText);
	JCheckBox cbUseOwnMultiplexer = new JCheckBox(gv.cbUseOwnDemultiplexerText);
	JCheckBox cbErrorChcking = new JCheckBox(gv.errorCheckingText);

	JLabel lblBarcodeStartPos = new JLabel("Barcode start position in the read");
	JLabel lblMaxHammingDist = new JLabel("Maximum Hamming distance between barcode and read");

	JRadioButton radioHuman = new JRadioButton("Homo sapiens");
	JRadioButton radioMouse = new JRadioButton("Mus musculus");
	ButtonGroup radioHumanMouseGroup = new ButtonGroup();

	JRadioButton radioTRA = new JRadioButton("Alpha chain (TRA)");
	JRadioButton radioTRB = new JRadioButton("Beta chain (TRB)");
	ButtonGroup radioTRATRBGroup = new ButtonGroup();

	JLabel lblHeadNr = new JLabel("Sample");
	JLabel lblHeadName = new JLabel("Name");
	JLabel lblHeadId = new JLabel("Identifier");
	JLabel lblHeadBarcode = new JLabel("Barcode");

	JCheckBox cbShowAdvancedOptions = new JCheckBox("Advanced options");

	JLabel lblParaBarcodeSplitter = new JLabel("Barcode splitter parameter");
	// JLabel lblParaMitcr = new JLabel("MiTcr parameter");
	JLabel lblParaMitcrTcrPackage = new JLabel("MiTCR (TCR-Package) parameter");
	JLabel lblParaPear = new JLabel("Pear parameter (-j is already set)");
	JLabel lblParaMixcrAlign = new JLabel("Mixcr Align parameter");
	JLabel lblParaMixcrAssemble = new JLabel("Mixcr Assemble parameter");
	JLabel lblParaMixcrExportClones = new JLabel("Mixcr ExportClones parameter");

	JButton btnStartPipeline = new JButton("Start");
//	JButton btnShowTutorialPdf = new JButton("Show Tutorial in Browser");

	

	public JPanel getPanel() {
		return panelConfig;
	}

	public PanelConfig(MainFrame mainFrame, JFrame outerFrame) {
		this.mainFrame = mainFrame;
		this.outerFrame = outerFrame;

		// actionListener are added only one time
		initComponents();
		// paint components
		// repaintConfigScreen();
	}

	private void initComponents() {
		// browse buttons
		btnChooseForwardReadFile.addActionListener(this);
		btnChooseReverseReadFile.addActionListener(this);
		btnChooseOutputFolder.addActionListener(this);

		// add and remove buttons
		btnAddSampleInputField.addActionListener(this);
		btnRemoveSampleInputField.addActionListener(this);

		btnRestoreDefaults.addActionListener(this);

		// auto complete check box
		cbAutoComplete.setSelected(gv.getAutoComplete());
		cbAutoComplete.addActionListener(new CheckBoxAutoCompleteListener());

		cbShowAdvancedOptions.setSelected(gv.getShowAdvancedOptions());
		cbShowAdvancedOptions.addActionListener(this);

		cbErrorChcking.setSelected(gv.errorChecking);
		cbErrorChcking.addActionListener(this);

		radioMouse.setSelected(true);

		radioHumanMouseGroup.add(radioHuman);
		radioHumanMouseGroup.add(radioMouse);

		radioHuman.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateMitcrTCRPackageSpeciesParameter();
			}
		});

		radioMouse.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateMitcrTCRPackageSpeciesParameter();
			}
		});

		radioTRB.setSelected(true);

		radioTRATRBGroup.add(radioTRA);
		radioTRATRBGroup.add(radioTRB);

		radioTRA.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateMitcrTCRPackageGeneParameter();
			}
		});

		radioTRB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateMitcrTCRPackageGeneParameter();
			}
		});

		if (GlobalVars.internalVersion)
			cbUseOwnMultiplexer.setSelected(gv.getUseOwnMultiplexer());
		else {
			gv.setUseOwnMultiplexer(true);
			cbUseOwnMultiplexer.setSelected(gv.getUseOwnMultiplexer());
		}

		cbUseOwnMultiplexer.addActionListener(this);

		// one for the model and for the GUI changes
		btnStartPipeline.addActionListener(new StartButtonListener());
		btnStartPipeline.addActionListener(this);
		
		//btnShowTutorialPdf.addActionListener(this);
	}

	@SuppressWarnings("unused")
	private void buildConfigScreen() {
		panelConfig.setLayout(new MigLayout());
		
		BufferedImage ClonoCalcIcon = new BufferedImage(1, 1, 1);
		try {
			ClonoCalcIcon = ImageIO.read(getClass().getResource("/images/ClonoCalc_BildText.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JLabel ClonoCalcIconLabel = new JLabel(new ImageIcon(ClonoCalcIcon));

		// auto complete check box
		panelConfig.add(cbAutoComplete);
		panelConfig.add(btnRestoreDefaults);
		panelConfig.add(ClonoCalcIconLabel ,"span 3 1, align right, wrap");


		

		panelConfig.add(separatortop, "growx, span");

		panelConfig.add(lblForwardRead);
		panelConfig.add(gv.txtForwardReadFile, "span 3");
		panelConfig.add(btnChooseForwardReadFile, "wrap");

		panelConfig.add(separatortop2, "growx, span");

		panelConfig.add(lblReverseRead);
		panelConfig.add(gv.txtReverseReadFile, "span 3");
		panelConfig.add(btnChooseReverseReadFile, "wrap");

		panelConfig.add(separatortop3, "growx, span");

		panelConfig.add(lblOutputFolder);
		panelConfig.add(gv.txtOutputFolder, "span 3");
		panelConfig.add(btnChooseOutputFolder, "wrap");
		
		
		// Sample selection
		panelConfig.add(separatorSample, "growx, span");
		

		panelConfigSamples.setLayout(new MigLayout());

		// add and remove buttons
		panelConfigSamples.add(btnAddSampleInputField);
			
		panelConfigSamples.add(btnRemoveSampleInputField, "wrap");
		
		
		
		

		// headline
		panelConfigSamples.add(lblHeadNr);
		panelConfigSamples.add(lblHeadName);
		panelConfigSamples.add(lblHeadId);
		panelConfigSamples.add(lblHeadBarcode, "wrap");

		// print rows
		for (int i = 0; i < gv.getNumberOfSamples(); i++) {
			panelConfigSamples.add(gv.getLblSamples(i));
			panelConfigSamples.add(gv.getTxtSamplesName(i));
			panelConfigSamples.add(gv.getTxtSamplesId(i));
			panelConfigSamples.add(gv.getTxtSamplesBarcode(i),"wrap");
		}
		

		
		panelConfig.add(panelConfigSamples, "wrap, span 3 1");

		panelConfigOptions.setLayout(new MigLayout());

		panelConfigOptions.add(lblBarcodeStartPos);
		panelConfigOptions.add(gv.txtBarcodeStartPos, "wrap");

		panelConfigOptions.add(lblMaxHammingDist);
		panelConfigOptions.add(gv.txtMaxHammingDist, "wrap");

		panelConfigOptions.add(radioHuman, "split 2");
		panelConfigOptions.add(radioMouse, "wrap");

		panelConfigOptions.add(radioTRA, "split 2");
		panelConfigOptions.add(radioTRB, "wrap");
		

		

		panelConfig.add(panelConfigOptions, "wrap, span 3 1");

		// show advanced options like parameters for binaries
		panelConfig.add(cbShowAdvancedOptions, "wrap");

		if (gv.getShowAdvancedOptions()) {
			panelAdvancedOptions.setLayout(new MigLayout());

			if (GlobalVars.internalVersion) {
				panelAdvancedOptions.add(cbUseOwnMultiplexer, "wrap");
				panelAdvancedOptions.add(cbErrorChcking, "wrap");
				if (!gv.getUseOwnMultiplexer()) {
					panelAdvancedOptions.add(lblParaBarcodeSplitter);
					panelAdvancedOptions.add(gv.txtParaBarcodeSplitter, "wrap");
				} else {

				}
			}
			// deprecated moved to mixcr
			// panelConfig.add(lblParaMitcr);
			// panelConfig.add(gv.txtParaMitcr, "wrap");
			// panelAdvancedOptions.add(lblParaMitcrTcrPackage);
			// panelAdvancedOptions.add(gv.txtParaMitcrTcrPackage, "wrap");
			if (GlobalVars.internalVersion) {
				panelAdvancedOptions.add(lblParaPear);
				panelAdvancedOptions.add(gv.txtParaPear, "wrap");
			}
			if (!GlobalVars.internalVersion || (GlobalVars.internalVersion && GlobalVars.internalVersionUseMixcr)) {
				panelAdvancedOptions.add(lblParaMixcrAlign);
				panelAdvancedOptions.add(gv.txtParaMixcrAlign, "wrap");
				panelAdvancedOptions.add(lblParaMixcrAssemble);
				panelAdvancedOptions.add(gv.txtParaMixcrAssemble, "wrap");
				panelAdvancedOptions.add(lblParaMixcrExportClones);
				panelAdvancedOptions.add(gv.txtParaMixcrExportClones, "wrap");
			}

			panelConfig.add(panelAdvancedOptions, "span x, wrap");
		}

		// one for the model and for the GUI changes
		panelConfig.add(btnStartPipeline);
		//panelConfig.add(btnShowTutorialPdf, "span 4 1, align right");

		
		
	}

	public void repaintConfigScreen() {
		// first remove all components from the panel
		panelConfig.removeAll();
		panelConfigSamples.removeAll();
		panelConfigOptions.removeAll();
		panelAdvancedOptions.removeAll();
		// add the components to the panel
		buildConfigScreen();

		outerFrame.pack();
	}

	public void actionPerformed(ActionEvent ae) {
		

		if (ae.getSource() == this.btnChooseForwardReadFile) {			

			// packed gz files are also supported (cmd: gunzip -c file)
			FileFilter filter = new FileNameExtensionFilter(fileTypeReadUnpacked + " or " + fileTypeReadPacked,
					fileTypeReadUnpacked, fileTypeReadPacked);
			File file = chooseFile("Select forward read", filter, false);
			if (file == null)
				return;

			if (file == null || file.getName().equals("")) {				
				gv.txtForwardReadFile.setText(gv.defaultTxtForwardReadFile);
				gv.txtForwardReadFile.setBackground(GlobalVars.colorDefault);
			} else {				
				gv.txtForwardReadFile.setText(file.getAbsolutePath().replace("\\", "/"));
				if (!file.getName().contains("R1"))
					gv.txtForwardReadFile.setBackground(GlobalVars.colorWarning);
				else {
					gv.txtForwardReadFile.setBackground(GlobalVars.colorValid);

					// try to auto complete the reverse read
					// do this only if autocompletion is on
					if (gv.getAutoComplete()) {
						String reverseReadGuess = file.getName().replace("R1", "R2");
						String reverseReadGuessGz = reverseReadGuess;
						if (!reverseReadGuess.substring(reverseReadGuess.length() - 3).equals(".gz"))
							reverseReadGuessGz += ".gz";

						File reverseFileGuess = HelpMethods.getFile(file, reverseReadGuess);
						if (reverseFileGuess.exists()) {
							gv.txtReverseReadFile.setText(reverseFileGuess.getAbsolutePath().replace("\\", "/"));
							gv.txtReverseReadFile.setBackground(GlobalVars.colorValid);
						} else {
							File reverseFileGuessGz = HelpMethods.getFile(file, reverseReadGuessGz);
							if (reverseFileGuessGz.exists()) {
								gv.txtReverseReadFile.setText(reverseFileGuessGz.getAbsolutePath().replace("\\", "/"));
								gv.txtReverseReadFile.setBackground(GlobalVars.colorValid);
							}
						}
						// set output folder
						gv.txtOutputFolder.setText(file.getParent().replace("\\", "/"));
					}
				}
			}

		} else if (ae.getSource() == this.btnChooseReverseReadFile) {

			FileFilter filter = new FileNameExtensionFilter(fileTypeReadPacked, fileTypeReadPacked, "zip");
			File file = chooseFile("Select reverse read", filter, false);
			if (file == null)
				return;

			if (file == null || file.getName().equals("")) {
				gv.txtReverseReadFile.setText(gv.defaultTxtReverseReadFile);
				gv.txtReverseReadFile.setBackground(GlobalVars.colorDefault);
			} else {
				gv.txtReverseReadFile.setText(file.getAbsolutePath().replace("\\", "/"));
				// check if file is unpacked and the forwardread
				if (!file.getName().contains("R2")) {
					gv.txtReverseReadFile.setBackground(GlobalVars.colorWarning);
				} else {
					gv.txtReverseReadFile.setBackground(GlobalVars.colorValid);
					if (gv.getAutoComplete()) {
						String forwardReadGuess = file.getAbsolutePath().replace("R2", "R1");
						String forwardReadGuess2 = forwardReadGuess.substring(0, forwardReadGuess.length() - 3);
						// System.out.println(forwardReadGuess);
						// System.out.println(forwardReadGuess2);

						if (new File(forwardReadGuess2).exists()) {
							gv.txtForwardReadFile.setText(forwardReadGuess2);
							gv.txtForwardReadFile.setBackground(GlobalVars.colorValid);
						} else if (new File(forwardReadGuess).exists()) {
							gv.txtForwardReadFile.setText(forwardReadGuess);
							gv.txtForwardReadFile.setBackground(GlobalVars.colorValid);
						}
						// set output folder
						gv.txtOutputFolder.setText(file.getParent().replace("\\", "/"));
					}
				}
			}

		} else if (ae.getSource() == this.btnChooseOutputFolder) {

			File file = chooseFile("Choose output folder", null, true);

			if (file == null || file.getAbsolutePath().equals("")) {
				gv.txtOutputFolder.setText(gv.defaultTxtOutputFolder);
				gv.txtOutputFolder.setBackground(GlobalVars.colorDefault);
			} else if (file.exists()) {
				gv.txtOutputFolder.setText(file.getAbsolutePath().replace("\\", "/"));
				gv.txtOutputFolder.setBackground(GlobalVars.colorValid);
			} else {
				gv.txtOutputFolder.setBackground(GlobalVars.colorWarning);
			}

		} else if (ae.getSource() == this.btnAddSampleInputField) {
			// System.out.println("btn pressed");
			gv.addSampleRow();
			repaintConfigScreen();
		} else if (ae.getSource() == this.btnRemoveSampleInputField) {
			gv.removeLastSampleRow();
			repaintConfigScreen();
		} else if (ae.getSource() == this.btnStartPipeline) {
			mainFrame.switchToLoadingScreen();
			// reset Debug output Area
			gv.txtpDebug.setText("");
//		} else if (ae.getSource() == this.btnShowTutorialPdf) { ////WORKING
//			mainFrame.showTutorialPdfInBrowser();			
		}else if (ae.getSource() == this.cbShowAdvancedOptions) {
			gv.setShowAdvancedOptions(cbShowAdvancedOptions.isSelected());
			invokeLaterRepaintConfigScreen();
		} else if (ae.getSource() == this.cbUseOwnMultiplexer) {
			gv.setUseOwnMultiplexer(cbUseOwnMultiplexer.isSelected());
			invokeLaterRepaintConfigScreen();
		} else if (ae.getSource() == this.cbErrorChcking) {
			gv.errorChecking = cbErrorChcking.isSelected();
			invokeLaterRepaintConfigScreen();
		} else if (ae.getSource() == this.btnRestoreDefaults) {
			gv.restoreDefaults();
			invokeLaterRepaintConfigScreen();
		}
	}

	private void invokeLaterRepaintConfigScreen() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				repaintConfigScreen();
			}
		});
	}

	private File chooseFile(String title, FileFilter filter, boolean folder) {
		System.out.println(gv.os);
		if (gv.os==OperatingSys.Linux||gv.os==OperatingSys.Windows) {
			if (!folder) { // use linux file dialog
				FileDialog d = new FileDialog(outerFrame);
				d.setTitle(title);
				d.setVisible(true);
				if (d.getFile() == null)
					return null;
				File selectedFile = new File(d.getDirectory(), d.getFile());
				// System.out.println(selectedFile.getAbsolutePath());
				return selectedFile;
			} else {

				// JFileChooser-Objekt erstellen
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle(title);

				// if (folder) {
				// disable the "All files" option.

				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				// } else {
				// chooser.addChoosableFileFilter(filter);
				// chooser.setFileFilter(filter);
				// }

				int rueckgabeWert = chooser.showOpenDialog(outerFrame);

				/* Abfrage, ob auf "Öffnen" geklickt wurde */
				if (rueckgabeWert == JFileChooser.APPROVE_OPTION) {
					// Ausgabe der ausgewaehlten Datei
					// System.out.println("Die zu oeffnende Datei ist: "
					// + chooser.getSelectedFile().getAbsolutePath());

					return chooser.getSelectedFile();
					// System.out.println(datei.canRead());
				}
				return null;
			}
		} else if(gv.os==OperatingSys.Mac){// if program runs on mac os //TODO !!!!!

			if (folder)
				System.setProperty("apple.awt.fileDialogForDirectories", "true");
			else
				System.setProperty("apple.awt.fileDialogForDirectories", "false");
			FileDialog d = new FileDialog(outerFrame);
			d.setTitle(title);
			d.setVisible(true);
			if (d.getFile() == null)
				return null;
			File selectedFile = new File(d.getDirectory(), d.getFile());		
			
			return selectedFile;
		}else{
			System.out.println("OS could not be detected");			
			return null;
		}
	}

	private void updateMitcrTCRPackageSpeciesParameter() {
		if (radioHuman.isSelected()) {
			gv.txtParaMitcrTcrPackage
					.setText(gv.txtParaMitcrTcrPackage.getText().replace("-species mm", "-species hs"));
			gv.txtParaMixcrAlign.setText(gv.txtParaMixcrAlign.getText().replace("-s mmu", "-s hsa"));
		} else {
			gv.txtParaMitcrTcrPackage
					.setText(gv.txtParaMitcrTcrPackage.getText().replace("-species hs", "-species mm"));
			gv.txtParaMixcrAlign.setText(gv.txtParaMixcrAlign.getText().replace("-s hsa", "-s mmu"));
		}
	}

	protected void updateMitcrTCRPackageGeneParameter() {
		if (radioTRA.isSelected()) {
			gv.txtParaMitcrTcrPackage.setText(gv.txtParaMitcrTcrPackage.getText().replace("-gene TRB", "-gene TRA"));
			gv.txtParaMixcrAlign.setText(gv.txtParaMixcrAlign.getText().replace("--loci TRB", "--loci TRA"));
		} else {
			gv.txtParaMitcrTcrPackage.setText(gv.txtParaMitcrTcrPackage.getText().replace("-gene TRA", "-gene TRB"));
			gv.txtParaMixcrAlign.setText(gv.txtParaMixcrAlign.getText().replace("--loci TRA", "--loci TRB"));
		}
	}
}
