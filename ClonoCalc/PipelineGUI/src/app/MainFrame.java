package app;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;



import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import sun.misc.Launcher;

public class MainFrame {

	JFrame frame = new JFrame("ClonoCalc");

	JPanel currentPanel;

	private enum ProgramState {
		config, running, finished
	};

	ProgramState programState = ProgramState.config;

	PanelFinished panelFinished = new PanelFinished(this, frame);
	PanelLoading panelLoading = new PanelLoading(this, frame);
	PanelConfig panelConfig = new PanelConfig(this, frame);

	public MainFrame() {
		frame.setMinimumSize(new Dimension(700, 400));
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setMaximumSize(new Dimension(screenSize.width, screenSize.height));
		// frame.setMaximumSize(new Dimension(600, 350));

		// add panelConfig only once and take changes into the panel
		panelConfig.repaintConfigScreen();
		frame.add(panelConfig.getPanel());
		currentPanel = panelConfig.getPanel();
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				showExitDialog(frame, programState);
			}
		});
		frame.setVisible(true);
	}

	public static void showExitDialog(Component c, ProgramState programState) {
		// distinguish current panel
		if (programState == ProgramState.config) {
			// if (JOptionPane.showOptionDialog(c,
			// "Save changes in configuration?", "Exit",
			// JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
			// null, null, JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION)
			System.exit(0);
		} else if (programState == ProgramState.running) {
			if (JOptionPane.showOptionDialog(c,
					"Current progress will be lost! Are you sure?", "Exit",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
					null, null, JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION)
				System.exit(0);
		} else if (programState == ProgramState.finished) {
			System.exit(0);
		}
	}

	// called by panelConfig, if start button pressed
	public void switchToLoadingScreen() {
		programState = ProgramState.running;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.remove(currentPanel);
				panelLoading.repaintLoadingScreen();
				frame.add(panelLoading.getPanel());
				currentPanel = panelLoading.getPanel();
				frame.pack();
				frame.setSize(700, 700);
			}
		});

	}

	// called by panelLoading, if cancel button pressed
	public void switchToConfigScreen() {
		programState = ProgramState.config;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.remove(currentPanel);
				panelConfig.repaintConfigScreen();
				frame.add(panelConfig.getPanel());
				currentPanel = panelConfig.getPanel();
				frame.pack();
			}
		});

	}
	
	public void showTutorialPdfInBrowser() {
		GlobalVars gv = GlobalVars.singleton();
		  try {			  
			File pdfFile;
			if(gv.isLinuxOS){
				pdfFile = new File("ClonoSuite-Tutorial.pdf");
			}else{
				pdfFile = new File(gv.userDir+"\\src\\ClonoSuite-Tutorial.pdf");
			}
			if (pdfFile.exists()) {
	
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(pdfFile);
				} else {
					System.out.println("Awt Desktop is not supported!");
				}
	
			} else {
				System.out.println("File does not exists!");
			}			
	
		  } catch (Exception ex) {
			ex.printStackTrace();
		  }
	
		}

	// called by bashExecution thread, if execution finished
	public void switchToFinishScreen() {
		programState = ProgramState.finished;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Point locOld = frame.getLocation();
				// int x = locOld.x;
				// int y = locOld.y;
				// Dimension dimold = frame.getSize();
				// frame.setLocation(x, y);
				frame.remove(currentPanel);
				panelFinished.repaintFinishedScreen();
				frame.add(panelFinished.getPanel());
				currentPanel = panelFinished.getPanel();
				SwingUtilities.updateComponentTreeUI(frame);

				// frame.setSize(600, 700);

				// frame.pack();
				// frame.setSize(dimold);
				// System.out.println(locOld);

				// frame.setLocation(100, y);
				// frame.setLocation(x, 100);

				// System.out.println(frame.getLocation());
			}
		});

	}

	public static void main(String[] args) throws InterruptedException {
		// init data
		HelpMethods.parseBarcodes(new File("illumina_barcodes.txt"));
		GlobalVars.singleton();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new MainFrame();
			}
		});
	}
}
