package Listener;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;

public class NameListener implements DocumentListener {

	@Override
	public void changedUpdate(DocumentEvent e) {
		nameValidation(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		nameValidation(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		nameValidation(e);
	}

	public void nameValidation(DocumentEvent e) {

		// get the owner of this document
		// JTextField owner = (JTextField) e.getDocument().getProperty("owner");

		GlobalVars gv = GlobalVars.singleton();

		for (int i = 0; i < gv.getNumberOfSamples(); i++) {
			JTextField txt = gv.getTxtSamplesName(i);
			txt.setBackground(GlobalVars.colorValid);
		}
		gv.detectDupsInTxtSampleNames();

	}

}
