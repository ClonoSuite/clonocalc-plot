package Listener;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;
import app.HelpMethods;

public class BarcodeListener implements DocumentListener {

	@Override
	public void changedUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		autocompleteIdOrBarcode(e);
	}

	public void autocompleteIdOrBarcode(DocumentEvent e) {
		// get the owner of this document
		// JTextField owner = (JTextField) e.getDocument().getProperty("owner");
		// get the neighbor of this object i.e. id or barcode field
		// JTextField neighbor = (JTextField) e.getDocument().getProperty(
		// "neighbor");

		// determine if it a Id or Barcode Field
		String type = (String) e.getDocument().getProperty("type");

		GlobalVars gv = GlobalVars.singleton();

		if (type.equals("barcode")) {

			for (int i = 0; i < gv.getNumberOfSamples(); i++) {
				JTextField owner = gv.getTxtSamplesBarcode(i);
				JTextField neighbor = (JTextField) owner.getDocument()
						.getProperty("neighbor");

				if (HelpMethods.isValidBarcode(owner.getText())) {
					// System.out.println(owner.getText());
					String id = HelpMethods.getIdFromBarcode(owner.getText());

					if (id == null) {
						owner.setBackground(GlobalVars.colorWarning);
					} else {
						// change id only if the id isn't correct
						// this is important, because otherwise there will be a
						// endless loop
						String neighborid = HelpMethods.parseId(neighbor
								.getText());
						if (GlobalVars.singleton().getAutoComplete()
								&& !id.equals(neighborid))
							neighbor.setText(id);
						owner.setBackground(GlobalVars.colorValid);
					}

				} else if (owner.getText().equals(""))
					owner.setBackground(GlobalVars.colorDefault);
				else {
					owner.setBackground(GlobalVars.colorInvalid);
				}
			}
			// proof for dups
			GlobalVars.singleton().detectDupsInTxtSampleBarcode();

		}

	}
}
