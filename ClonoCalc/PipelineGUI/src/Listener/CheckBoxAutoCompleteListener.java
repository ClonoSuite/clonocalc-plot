package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import app.GlobalVars;

public class CheckBoxAutoCompleteListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JCheckBox cb = (JCheckBox) arg0.getSource();
		// set the auto complete global variable
		if (cb.getText().equals(GlobalVars.singleton().cbAutoCompleteText))
			GlobalVars.singleton().setAutoComplete(cb.isSelected());
	}

}
