package Listener;

import java.io.File;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;

public class ReadsListener implements DocumentListener {

	@Override
	public void changedUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	public void readFileNameValidation(DocumentEvent e) {
		JTextField owner = (JTextField) e.getDocument().getProperty("owner");

		GlobalVars gv = GlobalVars.singleton();		
		
		if (owner == gv.txtForwardReadFile) {

			if (gv.txtForwardReadFile.getText().equals("")) {
				gv.txtForwardReadFile.setBackground(GlobalVars.colorDefault);
			} else if (new File(gv.txtForwardReadFile.getText()).exists()) {
				
				// check if file is the forward read
				if (!gv.txtForwardReadFile.getText().contains("R1")) {
					gv.txtForwardReadFile.setBackground(GlobalVars.colorWarning);
				} else {
					gv.txtForwardReadFile.setBackground(GlobalVars.colorValid);
				}

			} else {
				gv.txtForwardReadFile.setBackground(GlobalVars.colorInvalid);
			}


		} else if (owner == gv.txtReverseReadFile) {

			if (gv.txtReverseReadFile.getText().equals("")) {
				gv.txtReverseReadFile.setBackground(GlobalVars.colorDefault);
			} else if (new File(gv.txtReverseReadFile.getText()).exists()) {

				// check if file is the reverse read
				if (!gv.txtReverseReadFile.getText().contains("R2")) {
					gv.txtReverseReadFile.setBackground(GlobalVars.colorWarning);
				} else {
					gv.txtReverseReadFile.setBackground(GlobalVars.colorValid);
				}

			} else {
				gv.txtReverseReadFile.setBackground(GlobalVars.colorInvalid);
			}

		} else if (owner == gv.txtOutputFolder) {
			File path = new File(gv.txtOutputFolder.getText());
			if (gv.txtOutputFolder.getText().equals("")) {
				gv.txtOutputFolder.setBackground(GlobalVars.colorDefault);
			} else if (path.exists() && path.isDirectory()) {
				gv.txtOutputFolder.setBackground(GlobalVars.colorValid);
			} else {
				gv.txtOutputFolder.setBackground(GlobalVars.colorInvalid);
			}

		}

	}

}
