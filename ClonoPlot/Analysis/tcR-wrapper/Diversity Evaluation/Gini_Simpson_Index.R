#'IMPORTANT INFORMATION
#'This is only wrapper script for a function from the R tcR-package.
#'To get more details about the tcR-package please visit
#'https://cran.r-project.org/web/packages/tcR/index.html
#'or
#'http://imminfo.github.io/tcr/
#'---------------------------------------------------------------------------------------------
#' @description
#' Gini Simpson Index on read count in a barplot. (INFO: This is a wrapper script for a function from the R tcR-package)
#' 
#' @param title string() Set the title of the plot
#' @param width float() Set the width of the diagram.
#' @param height float() Set the height of the diagram.

Gini_Simpson_Index <- function(dataFrameList, height = 5, width = 15, title="Gini-Simpson index") {
  for(n in names(dataFrameList)) {  
    dataFrameList[[n]] <- renameToTcR2(dataFrameList[[n]])
  }
  if(width == 15)
		width = 1 * length(dataFrameList)
	# Evaluate the skewness of clonal distribution.
	repDiversity(dataFrameList, 'gini.simp', 'read.prop')
	div <- sapply(dataFrameList, function (x) gini.simpson(x$Read.proportion))

	d <- data.frame(x=names(dataFrameList), Value=div)
	plot <- ggplot(d, aes(x, Value)) + geom_bar(stat="identity") +
	  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + labs(x="")
	
	return(plot)
	
	#pdf(file = paste0(title, ".pdf"), height = height, width = width)
	#par(mar=c(10,4,1.5,0))
	#barplot(div, main=title, xlab="", las=2)
	#dev.off()
}
