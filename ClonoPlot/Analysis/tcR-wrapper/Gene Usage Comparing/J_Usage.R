#'IMPORTANT INFORMATION
#'This is only wrapper script for a function from the R tcR-package.
#'To get more details about the tcR-package please visit
#'https://cran.r-project.org/web/packages/tcR/index.html
#'or
#'http://imminfo.github.io/tcr/
#'---------------------------------------------------------------------------------------------
#' @description
#' Plot a histogram or a grid of histograms of J-usage. (INFO: This is a wrapper script for a function from the R tcR-package)
#' 
#' @param title string() Set the title of the plot
#' @param width float() Set the width of the diagram.
#' @param height float() Set the height of the diagram.
#' @param norm bool() If TRUE than compute the normalised number.
#' @param ncol integer() Number of columns in a grid of histograms if dodge is FALSE.
#' @param coord.flip bool() if TRUE then flip coordinates.
#' @param labs Character vector of length 2 with names for x-axis and y-axis.
#' @param dodge bool() If this is TRUE plot J-usage for all data frames to the one histogram.

J_Usage <- function(dataFrameList, height = 5, width = 15, title="J-usage-control", norm=TRUE, dodge=TRUE, ncol=2, coord.flip = FALSE, labs = c("Gene", "Frequency")) {
	#make width suitable for the number of lists, if width is not manually set
  for(n in names(dataFrameList)) {  
    dataFrameList[[n]] <- renameToTcR2(dataFrameList[[n]])
  }
	if(width == 15)
		width = 2 * length(dataFrameList)
	if(height == 5) {
		height = length(dataFrameList)/2
		if(height < 4)
			height = 4
	}
  #remember dodge == TRUE => ggplot, dodge == FALSE => no ggplot !!!
  if(dodge){
	  plot <- vis.gene.usage(dataFrameList, .main = title, .genes = MOUSE_J, .dodge = dodge, .norm=norm)
	  return(plot)
  } else{
    dev.control(displaylist="enable")
    vis.gene.usage(dataFrameList, .main = title, .genes = MOUSE_J, .dodge = dodge, .ncol = ncol, .coord.flip = coord.flip, .labs = labs, .norm=norm)
    return(recordPlot())
  }
}
