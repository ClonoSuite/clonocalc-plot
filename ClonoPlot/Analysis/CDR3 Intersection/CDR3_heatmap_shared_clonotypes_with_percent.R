#' @description
#' Plot a heatmap with percentage
#' 
#' @param marginBottom integer() Set margin on the bottom. 
#' @param marginSide integer() Set the margin on the side.
#' @param title string() Set the title of the plot.
#' @param width integer() Set the width of the diagram.
#' @param height integer() Set the height of the diagram.
#' @param fontColorMap string() Set the font Color for text in the map.
#' @param fontSizeMap integer() Set the font size for the text in the map.
#' @param fontSizeSide integer() Set the font size of the side labels.
#' @param fontSizeBottom integer() Set the font size of the bottom labels.
#' @param filename string() Filename is set => data table is stored to a file named by filename.

CDR3_heatmap_shared_clonotypes_with_percent <- function(dataFrameList, height = 10, width = 12, marginBottom = 13, marginSide = 13, fontSizeBottom = 2, fontSizeSide = 2, fontSizeMap = 2,title = "Heatmap shared percent ", fontColorMap = "black", filename=NULL) {

    l <- dataFrameList

    m <- matrix(NA, nrow = length(l), ncol = length(l))
    rownames(m) <- colnames(m) <- names(l)
    for (n1 in names(l)) {
        for (n2 in names(l)) {
            v.index <- l[[n1]][, "CDR3.amino.acid.sequence"] %in% l[[n2]][, "CDR3.amino.acid.sequence"]
            v <- sum(v.index)
            m[n1, n2] <- v
        }
    }

    m.percentage <- m
    for (i in 1:nrow(m.percentage)) {
        m.percentage[i, ] <- paste0(m[i, ], " (", round(m[i, ] * 100/m[i, i], 2),
            "%)")
    }

    if (!is.null(filename))
      write.table(m, sep = ";", file = filename, col.names = T, row.names = F)

    # Plot a heatmap of the number of shared clonotypes in orange.
    # l.rownames<-sapply(l,rownames)
    dev.control(displaylist = "enable")

    heatmap.2(-m, cellnote = m.percentage, trace = "none", margins = c(marginBottom,
        marginSide), cexCol = fontSizeBottom, cexRow = fontSizeSide, notecex = fontSizeMap,
        notecol = fontColorMap, na.color = par("bg"))

    return(recordPlot())
}
