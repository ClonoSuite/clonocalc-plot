package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import app.BashExecution;
import app.GlobalVars;
import util.ScriptParser.Script;

public class CreateScript {
	private static final Logger log = Logger.getLogger(CreateScript.class);

	GlobalVars gv = GlobalVars.singleton();

	public final static String tmpListName = "tmpList";
	public final static String prefix = "id_";
	public final static String initFuncPath = "DO_NOT_DELETE_init_function.txt";
	public final static String saveFuncPath = "DO_NOT_DELETE_save_function.txt";

	public final static String[] rLibrarys = new String[] { "tcR", "data.table", "gplots", "ggplot2", "stringr",
			"RCircos", "dplyr" };
	public final static String[] rSkripts = new String[] { "DO_NOT_DELETE_funcs_tcr.RData",
			"DO_NOT_DELETE_renameToTcR2.RData", "DO_NOT_DELETE_alphabets.RData" };

	public void createSkript() {
		List<String> selectedTables = gv.getSelectedTables();
		List<String> selectedTablesNames = gv.getSelectedTablesNames();
		List<Script> selectedScriptsClass = gv.getSelectedScriptsClass();
		String outputDirPath = gv.txtOutputFolder.getText();

		final JScrollPane spTxtpDebug = GlobalVars.singleton().spTxtpDebug;

		BashExecution.append(spTxtpDebug, "OUTPUT FOLDER:\n", null);
		BashExecution.append(spTxtpDebug, outputDirPath + "\n", null);
		printListToTextPane(spTxtpDebug, selectedTables, "SELECTED TABLES:");
		printListToTextPane(spTxtpDebug, selectedTablesNames, "SELECTED TABLES NAMES:");
		printListToTextPane(spTxtpDebug, gv.getSelectedScripts(), "SELECTED SCRIPTS AND PARAMETERS:");
		BashExecution.append(spTxtpDebug, "", null);

		try {
			FileWriter fw = new FileWriter(gv.tmpFolder + File.separator + gv.rscriptFileName);
			BufferedWriter bw = new BufferedWriter(fw);

			// load libraries
			for (String rCodeLoadLibrary : getRCodeLoadLibrarys()) {
				bw.write(rCodeLoadLibrary);
			}

			// print loaded librarys to console
			String getVersions = "cat(R.Version()$version.string,\" platform:\",R.Version()$platform,\"\\n\")\n";
			getVersions += "cat(sapply(sessionInfo()$loadedOnly, function(x) paste0(x$Package, \" \", x$Version, \", \")),\"\\n\")\n";
			getVersions += "cat(sapply(sessionInfo()$otherPkgs, function(x) paste0(x$Package, \" \", x$Version, \", \")),\"\\n\")";
			bw.write(getVersions);

			bw.newLine();

			// load scripts
			for (String rCodeLoadScript : getRCodeLoadScripts())
				bw.write(rCodeLoadScript);

			bw.newLine();

			// set working dir
			bw.write("setwd(\"" + outputDirPath.replace("\\", "\\\\") + "\")");

			bw.newLine();

			// read tables
			for (String rCodeReadTable : getRCodeReadTables(selectedTables))
				bw.write(rCodeReadTable);

			bw.newLine();

			// create R list with tables
			bw.write(getRCodeCreateList(selectedTablesNames));

			// preprocess list
			bw.write(getRCodePreprocessingList(gv, true));

			// import selected scripts
			for (String rCodeSourceScriptFunction : getRCodeSourceScriptFunctions(selectedScriptsClass))
				bw.write(rCodeSourceScriptFunction);

			bw.newLine();

			// create device
			bw.write("if(names(dev.cur()) == \"null device\")\n" + "    pdf(file=\"tmp729384728947.pdf\")\n");

			// execute script functions
			for (String rCodeExecuteScriptFunction : getRCodeExecuteScriptFunctions(gv))
				bw.write(rCodeExecuteScriptFunction);

			bw.write("cat(\"" + gv.executionFinishedStr + "\")\n");

			bw.close();
		} catch (IOException e) {
			log.error(e, e);
		}

	}

	private void printListToTextPane(JScrollPane jScrollPane, List<String> list, String introductionLine) {
		if (introductionLine != null && !introductionLine.equals(""))
			BashExecution.append(jScrollPane, introductionLine + "\n", null);
		for (String entry : list) {
			BashExecution.append(jScrollPane, entry + "\n", null);
		}
	}

	public static String[] getRCodeLoadLibrarys() {
		String[] code = new String[rLibrarys.length];

		for (int i = 0; i < rLibrarys.length; i++) {
			code[i] = "result = tryCatch({\n";
			code[i] += "suppressMessages(library(" + rLibrarys[i] + "))\n";
			code[i] += "cat(\"load library " + rLibrarys[i] + "\\n\")\n";
			code[i] += "}, error = function(e) {\n";
			code[i] += "  print(e)\n";
			code[i] += "  cat(\"Try to install the package " + rLibrarys[i] + " ...\\n\")\n";
			// choose mirror with index 40, to avoid a popup
			code[i] += "  chooseCRANmirror(ind=40)\n";
			code[i] += "  install.packages(\"" + rLibrarys[i] + "\", dependencies = T)\n";
			// try to load again
			code[i] += "suppressMessages(library(" + rLibrarys[i] + "))\n";
			code[i] += "cat(\"load library " + rLibrarys[i] + "\\n\")\n";
			code[i] += "})\n";
		}
		return code;
	}

	public static String[] getRCodeLoadScripts() {
		String[] code = new String[rSkripts.length];

		for (int i = 0; i < rSkripts.length; i++) {
			code[i] = "load(\""
					+ (GlobalVars.mainScriptsDirAbsolutePath + File.separator + rSkripts[i]).replace("\\", "\\\\")
					+ "\")\n";
		}
		return code;
	}

	public static String[] getRCodeReadTables(List<String> selectedTables) {
		String[] code = new String[selectedTables.size()];
		// id_0 <-
		// data.frame(fread("/home/peter/tables/tcr/id_K_7874_i2_TCTGAC_final.csv",
		// header=T, stringsAsFactors=F))

		for (int i = 0; i < selectedTables.size(); i++) {
			code[i] = prefix + Integer.toString(i) + " <- data.frame(fread(\""
							 + selectedTables.get(i).replace("\\", "\\\\") + "\", header=T, stringsAsFactors=F))\n";
		}
		return code;
	}

	public static String getRCodeCreateList(List<String> selectedTablesNames) {
		// tmpList<-list(i9_1800_t=ATGATA_1800_t_i9,
		// i11_1855_t=TACGTA_1855_t_i11,
		// i10_1855_k=CGTGAT_1855_k_i10, i12_1855_b=TGAGCG_1855_b_i12,
		// i4_1856_t=CGCTCT_1856_t_i4, i3_1856_k=TGACTA_1856_k_i3,
		// i5_1856_b=AGATGA_1856_b_i5)

		String code = "tmpList <- list(";
		for (int i = 0; i < selectedTablesNames.size(); i += 2) {
			code += "\"" + selectedTablesNames.get(i + 1).replace("\\", "\\\\") + "\" = " + CreateScript.prefix
					+ Integer.toString(i / 2);
			if (i + 1 < selectedTablesNames.size() - 1)
				code += ",\n";
			else
				code += ")\n";
		}
		return code;
	}

	public static String getRCodeUpdateNamesList(List<String> selectedTablesNames) {
		String code = "names(tmpList) <- c(";
		for (int i = 0; i < selectedTablesNames.size(); i += 2) {
			code += "\"" + selectedTablesNames.get(i + 1).replace("\\", "\\\\") + "\"";
			if (i + 1 < selectedTablesNames.size() - 1)
				code += ",\n";
			else
				code += ")\n";
		}

		return code;
	}

	public static String getRCodePreprocessingList(GlobalVars gv, boolean save) {
		// set used species alphabet
		String code = "\n" + "speciesAlphabet <- \"" + gv.getUsedSpeciesAlphabet() + "\"\n";

		code += "\n" + FileOperations.readFile(GlobalVars.mainScriptsDirAbsolutePath + File.separator + initFuncPath);

		// cut sutff
		code += "\n\n";
		code += "for(n in names(" + tmpListName + ")) {\n";
		code += "\t" + tmpListName + "[[n]] <- " + tmpListName + "[[n]][(" + tmpListName + "[[n]][,1] >= "
				+ gv.getCutReadCount() + "), ]\n";
		// user input is [0-100], background use [0-1]
		code += "\t" + tmpListName + "[[n]] <- " + tmpListName + "[[n]][(" + tmpListName + "[[n]][,2] >= "
				+ (gv.getCutReadPercent() / 100) + "), ]\n";
		// norm again
		code += "\t" + tmpListName + "[[n]][,2] <- " + tmpListName + "[[n]][,2] / sum(" + tmpListName + "[[n]][,2])\n";
		code += "\tassign(n, " + tmpListName + "[[n]])\n";
		code += "}\n\n";

		if (save) {
			// add rscript.R path as variable to script
			code += "scriptPath <- \"" + (GlobalVars.scriptParentDirAbsolutePath + File.separator + gv.tmpFolder
					+ File.separator + gv.rscriptFileName).replace("\\", "\\\\") + "\"\n";
			// save list, workspace and script to output folder
			code += FileOperations.readFile(GlobalVars.mainScriptsDirAbsolutePath + File.separator + saveFuncPath)
					+ "\n\n";
		}

		return code;
	}

	public static String[] getRCodeSourceScriptFunctions(List<Script> selectedScripts) {
		String[] code = new String[selectedScripts.size()];
		// source("script.r")
		for (int i = 0; i < selectedScripts.size(); i++)
			code[i] = "source(\"" + selectedScripts.get(i).getAbsolutePathToScript().replace("\\", "\\\\") + "\")\n";

		return code;
	}

	public static String[] getRCodeExecuteScriptFunctions(GlobalVars gv) {
		List<Script> selectedScripts = gv.getSelectedScriptsClass();
		if (selectedScripts.size() == 0)
			return new String[] {};
		String[] code = new String[selectedScripts.size()];
		// pdf(file = paste0(title, ".pdf"), height = height, width = width)
		// print(myplot)
		// dev.off()
		// String command = "pdf(file = paste0(\"" + title + "\", \".pdf\"),
		// height = " + height + ", width = " + width
		// + ");" + "print(" + plotObjectName + ");" + "dev.off();";

		// testfunction(tmpList, width = 8, height = 9)
		// call script functions with user parameter
		// !!! warning first argument must be the dataframe !!!
		// result = tryCatch({
		// expr
		// }, warning = function(w) {
		// print(e)
		// }, error = function(e) {
		// print(e)
		// })

		List<String> scriptNamesList = new ArrayList<String>();

		code[0] = "counter <- 0\n";
		for (int i = 0; i < selectedScripts.size(); i++) {
			Script script = selectedScripts.get(i);
			code[i] += "result = tryCatch({\n";
			code[i] += "	counter <- counter + 1\n";
			String filename = new File(script.getAbsolutePathToScript()).getName();
			String filenamewoex = FileOperations.getNamewoExtension(filename);
			scriptNamesList.add(filenamewoex);
			code[i] += "	cat(\"-------------------------------------------------------------------------\\n\")\n";
			code[i] += "	cat(\"" + filenamewoex + "\\n\")\n";

			// if (GlobalVars.os == OperatingSys.Linux)
			code[i] += "	p <- (";
			// else
			// code[i] += " ";
			code[i] += filenamewoex + "(tmpList" + script.getParameterLine();
			// if (GlobalVars.os == OperatingSys.Linux)
			code[i] += "))\n";
			// else
			// code[i] += ")\n";

			// pdf(file = "file.pdf", height = hh, width = ww)
			code[i] += "	pdf(file = paste0(\"" + gv.getPrefixNameForAllPlots()
					+ cutFirstLastChar(script.tryToGetValue("title")) + gv.getSuffixNameForAllPlots()
					+ "\", \".pdf\"),height = " + script.tryToGetValue("height") + ", width = "
					+ script.tryToGetValue("width") + ")\n";

			code[i] += "	print(p)\n";

			code[i] += "	dev.off()\n";
			code[i] += "	cat(paste0(\"" + GlobalVars.doneScriptStr + "\",counter,\"\\n\"))\n";
			code[i] += "}, error = function(e) {\n";
			code[i] += "	print(e)\n";
			code[i] += "	cat(paste0(\"" + GlobalVars.failedScriptStr + "\",counter,\"\\n\"))\n";
			code[i] += "})\n";
		System.out.println(code[i]);	
		}

		code[code.length - 1] += "graphics.off()\n";
		code[code.length - 1] += "file.remove(\"tmp729384728947.pdf\")\n";

		gv.setSelectedScriptRFuncNames(scriptNamesList);

		return code;
	}

	private static String cutFirstLastChar(String str) {
		return str.substring(1, str.length() - 1);
	}
}
