package util;

import org.apache.log4j.Logger;

public class OSDetection {
	private static final Logger log = Logger.getLogger(OSDetection.class);

	private static String strOS = System.getProperty("os.name").toLowerCase();

	public enum OperatingSys {
		Linux, Mac, Windows, Unsupported
	};

	public static OperatingSys getOperatingSys() {	
//		ProcessBuilder builder = new ProcessBuilder("which","Rscript");
//		builder.redirectErrorStream(true);
//		try {
//			Process proc = builder.start();
//			InputStream stdout = proc.getInputStream();
//			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
//
//			String line;
//			while ((line = reader.readLine()) != null) {
//				log.info(line);
//			}
//		} catch (IOException e) {	
//			log.error(e,e);
//		}
		
		log.info("Java Version is " + System.getProperty("java.version"));
		log.info("Java Home is " + System.getProperty("java.home"));
		log.info("OS Arch is " + System.getProperty("os.arch") + " / OS Version is " + System.getProperty("os.version"));
		String osName = System.getProperty("os.name");
		if (isWindows()) {
			log.info("OS is Windows (" + osName + ")");
			return OperatingSys.Windows;
		} else if (isMac()) {
			log.info("OS is Mac (" + osName + ")");
			return OperatingSys.Mac;
		} else if (isUnix()) {
			log.info("OS is Unix or Linux (" + osName + ")");
			return OperatingSys.Linux;
		} else if (isSolaris()) {
			log.info("Solaris is not supported! (" + osName + ")");
		} else {
			log.info("Unknown OS! (" + osName + ")");
		}
		return OperatingSys.Unsupported;
	}

	public static boolean isWindows() {
		return (strOS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (strOS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (strOS.indexOf("nix") >= 0 || strOS.indexOf("nux") >= 0 || strOS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (strOS.indexOf("sunos") >= 0);
	}

}
