package util;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class TimeLogger {
	private static final Logger log = Logger.getLogger(TimeLogger.class);

	private String[] timeStampsNames;
	private long[] timeStamps;
	private int counter;

	public TimeLogger(String[] timeStampsNames) {
		this.timeStampsNames = timeStampsNames;
		timeStamps = new long[timeStampsNames.length + 1];
		timeStamps[0] = System.nanoTime();
		counter = 1;
	}

	public String getTimeInM_S_MS(long startTime, long endTime) {
		long elapsedTime = endTime - startTime;
		long elapsedMilliseconds = TimeUnit.MILLISECONDS.convert(elapsedTime,
				TimeUnit.NANOSECONDS);
		long milliseconds = elapsedMilliseconds % 1000;
		elapsedMilliseconds /= 1000;
		long seconds = elapsedMilliseconds % 60;
		long minutes = elapsedMilliseconds / 60;

		return minutes + "m " + seconds + "s " + milliseconds + "ms";
	}

	public boolean measureTime() {
		if (counter < timeStamps.length) {
			timeStamps[counter++] = System.nanoTime();
			return true;
		}
		return false;
	}

	public String getReport() {
		String report = "";
		for (int i = 0; i < timeStamps.length - 1; i++) {
			report += timeStampsNames[i] + " "
					+ getTimeInM_S_MS(timeStamps[i], timeStamps[i + 1]) + "\n";
		}
		log.debug(report);
		return report;
	}
}
