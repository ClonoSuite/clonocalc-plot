package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.log4j.Logger;

public class SerializeGeneric<T extends Serializable> {
	private static final Logger log = Logger.getLogger(SerializeGeneric.class);

	private String fileAbsolutePath;
	private T serObject;

	public SerializeGeneric(String fileAbsolutePath, T serObject) {
		super();
		this.fileAbsolutePath = fileAbsolutePath;
		this.serObject = serObject;
	}

	public SerializeGeneric(String fileAbsolutePath) {
		super();
		this.fileAbsolutePath = fileAbsolutePath;
	}

	public void serializeObject() {
		try {
			FileOutputStream file = new FileOutputStream(fileAbsolutePath);
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(serObject);
			out.close();
			file.close();
		} catch (Exception e) {
			log.debug(e, e);
		}
	}

	@SuppressWarnings("unchecked")
	public T deserializeObject() {
		T object = null;
		try {
			FileInputStream file = new FileInputStream(fileAbsolutePath);
			ObjectInputStream in = new ObjectInputStream(file);
			object = (T) in.readObject();
			in.close();
			file.close();
		} catch (Exception e) {
			log.debug(e, e);
		}
		return object;
	}
}
