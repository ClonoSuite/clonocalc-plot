package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public class Gzip {
	private static final Logger log = Logger.getLogger(Gzip.class);

	public static void gzipFile(String sourceFilePath, String destGzipPath) {

		log.debug("Gzip folder " + sourceFilePath + " to " + destGzipPath);

		byte[] buffer = new byte[1024];

		try {

			GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(destGzipPath));

			FileInputStream in = new FileInputStream(sourceFilePath);

			int len;
			while ((len = in.read(buffer)) > 0) {
				gzos.write(buffer, 0, len);
			}

			in.close();

			gzos.finish();
			gzos.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void zipFileOrDir(String sourceDirPath, String destZipPath) {
		// Create a stream to compress data and write it to the zipfile
		ZipOutputStream out;
		try {
			out = new ZipOutputStream(new FileOutputStream(destZipPath));
			rekDirZip(new File(sourceDirPath), "", out);
			out.close();
		} catch (IOException e) {
			log.error(e, e);
		}
	}

	private static void rekDirZip(File fileDir, String destPathInZip, ZipOutputStream out) {
		byte[] buffer = new byte[4096];
		int bytes_read;

		if (!fileDir.exists()) {
			log.error("file " + fileDir.getAbsolutePath() + " does not exist!");
			return;
		}

		if (fileDir.isDirectory()) {
			String[] entries = fileDir.list();
			for (int i = 0; i < entries.length; i++) {
				File f = new File(fileDir, entries[i]);
				rekDirZip(f, destPathInZip + File.separator + fileDir.getName(), out); // Support
																						// sub-directories
			}
		} else {
			FileInputStream in;
			try {
				in = new FileInputStream(fileDir);
				// file
				ZipEntry entry = new ZipEntry(destPathInZip + File.separator + fileDir.getName()); // Make
																									// a
				// ZipEntry
				out.putNextEntry(entry); // Store entry
				while ((bytes_read = in.read(buffer)) != -1)
					// Copy bytes
					out.write(buffer, 0, bytes_read);
				in.close(); // Close input stream
			} catch (IOException e) {
				log.error(e, e);
			}
		}
	}

	public static void encryptZipFileDirList(HashMap<String, String> fileDirMap, String destZipPath, Krypto krypto) {
		// Create a stream to compress data and write it to the zipfile
		ZipOutputStream out;
		try {
			if (krypto == null)
				out = new ZipOutputStream(new FileOutputStream(destZipPath));
			else
				out = (new ZipOutputStream(krypto.encryptOutputStream(new FileOutputStream(destZipPath))));
			for (Entry<String, String> entry : fileDirMap.entrySet()) {
				String source = entry.getKey();
				String dest = entry.getValue();
				rekDirZip(new File(source), dest, out);
			}

			out.close();
		} catch (Exception e) {
			log.error(e, e);
		}
	}

	public static void decryptZip(String destZipPath, String sourceEncryptedZipPath, Krypto krypto) {
		InputStream in;
		OutputStream out;
		try {
			in = krypto.decryptInputStream(new FileInputStream(sourceEncryptedZipPath));
			out = new FileOutputStream(destZipPath);
			copyStream(in, out);
			in.close();
			out.close();
		} catch (Exception e) {
			log.error(e, e);
		}
	}

	public static void copyStream(InputStream input, OutputStream output) throws IOException {
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}
}
