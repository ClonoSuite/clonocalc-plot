package util;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

//http://blog.axxg.de/java-aes-verschluesselung-mit-beispiel/
//@Alexander Gräsel

public class Krypto {
	private static final Logger log = Logger.getLogger(Krypto.class);

	private Key key = null;
	private String verfahren = null;

	/**
	 * @param Key
	 *            verwendeter Schluessel
	 * @param verfahren
	 *            bestimmt das verwendete Verschluesselungsverfahren "RSA",
	 *            "AES", ....
	 * @throws Exception
	 */
	public Krypto(String password, String verfahren) throws Exception {
		this.key = generateKey(password, verfahren);
		this.verfahren = verfahren;
	}

	public Key generateKey(String password, String algorithm) {
		try {
			// byte-Array erzeugen
			byte[] key = (password).getBytes("UTF-8");
			// aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
			MessageDigest sha = MessageDigest.getInstance("MD5");
			key = sha.digest(key);
			// nur die ersten 128 bit nutzen
			key = Arrays.copyOf(key, 16);
			// der fertige Schluessel
			// SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
			return new SecretKeySpec(key, algorithm);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			log.error(e, e);
		}
		return null;
	}

	/**
	 * Verschluesselt einen Outputstream
	 * 
	 * @param os
	 *            Klartext-Outputstream
	 * @return verschluesselter Outputstream
	 * @throws Exception
	 */
	public OutputStream encryptOutputStream(OutputStream os) throws Exception {
		// integritaet pruefen
		valid();

		// eigentliche Nachricht mit RSA verschluesseln
		Cipher cipher = Cipher.getInstance(verfahren);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		os = new CipherOutputStream(os, cipher);

		return os;
	}

	/**
	 * Entschluesselt einen Inputstream
	 * 
	 * @param is
	 *            verschluesselter Inputstream
	 * @return Klartext-Inputstream
	 * @throws Exception
	 */
	public InputStream decryptInputStream(InputStream is) throws Exception {
		// integritaet pruefen
		valid();

		// Daten mit AES entschluesseln
		Cipher cipher = Cipher.getInstance(verfahren);
		cipher.init(Cipher.DECRYPT_MODE, key);
		is = new CipherInputStream(is, cipher);

		return is;
	}

	// public InputStream encryptZipFile(String filePath) {
	// try {
	// InputStream inputstream = new FileInputStream(filePath);
	// } catch (FileNotFoundException e) {
	// log.error(e, e);
	// }
	// }

	/**
	 * Verschluesselt einen Text in byte
	 * 
	 * @param text
	 *            Klartext
	 * @return BASE64 String
	 * @throws Exception
	 */

	public byte[] encrypt(String text) throws Exception {
		// integritaet pruefen
		valid();

		// Verschluesseln
		Cipher cipher = Cipher.getInstance(verfahren);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(text.getBytes());

		// bytes zu Base64-String konvertieren
		// BASE64Encoder myEncoder = new BASE64Encoder();
		// String geheim = myEncoder.encode(encrypted);

		return encrypted;
	}

	/**
	 * Entschluesselt einen byte kodierten Text
	 * 
	 * @param geheim
	 *            BASE64 kodierter Text
	 * @return Klartext
	 * @throws Exception
	 */
	public String decrypt(byte[] crypted) throws Exception {
		// integritaet pruefen
		valid();

		// BASE64 String zu Byte-Array
		// BASE64Decoder myDecoder = new BASE64Decoder();
		// byte[] crypted = myDecoder.decodeBuffer(geheim);

		// entschluesseln
		Cipher cipher = Cipher.getInstance(verfahren);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] cipherData = cipher.doFinal(crypted);
		return new String(cipherData);
	}

	// ++++++++++++++++++++++++++++++
	// Validierung
	// ++++++++++++++++++++++++++++++

	private boolean valid() throws Exception {
		if (verfahren == null) {
			throw new NullPointerException("Kein Verfahren angegeben!");
		}

		if (key == null) {
			throw new NullPointerException("Keinen Key angegeben!");
		}

		if (verfahren.isEmpty()) {
			throw new NullPointerException("Kein Verfahren angegeben!");
		}

		return true;
	}

	// ++++++++++++++++++++++++++++++
	// Getter und Setter
	// ++++++++++++++++++++++++++++++

	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public String getVerfahren() {
		return verfahren;
	}

	public void setVerfahren(String verfahren) {
		this.verfahren = verfahren;
	}

}
