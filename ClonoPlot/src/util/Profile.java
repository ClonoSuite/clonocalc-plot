package util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import app.GlobalVars;

public class Profile {
	private static final Logger log = Logger.getLogger(Profile.class);

	private final static String profileFolderPath = GlobalVars.singleton().profilesFolder + File.separator;

	public static void saveCurrentProfile(String profileName, boolean askForOverwrite) {
		log.debug("begin profile save " + profileName);
		// save actions are called by panelconfig
		File profilePath = new File(profileFolderPath + profileName);

		if (askForOverwrite && profilePath.exists()) {
			// ask for overwriting existing profile
			int reply = JOptionPane.showConfirmDialog(null, "Overwrite profile " + profileName + "?",
					"Profile already exists.", JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.NO_OPTION) {
				return;
			}
		} else {
			// also creates the profile dir
			profilePath.mkdirs();
			if (!profilePath.isDirectory()) {
				log.debug(profilePath.getAbsolutePath() + " is not a dir!");
				return;
			}
		}
		// save the current profile
		copyProfile(GlobalVars.singleton().tmpFolder, profilePath.getAbsolutePath());
		log.debug("save " + profileName + " in " + profilePath.getAbsolutePath());
	}

	public static boolean loadProfile(String profileName) {
		log.debug("try to load profile " + profileName);
		if (!profileExists(profileName)) {
			log.debug("profile " + profileName + " does not exsist.");
			return false;
		}

		// backup old file

		// if profile exists exchange tmp files
		copyProfile(profileFolderPath + profileName, GlobalVars.singleton().tmpFolder);
		return true;
	}

	private static void copyProfile(String sourcePath, String destPath) {
		// get all files in the tmp dir
		String[] entries = new File(sourcePath).list();
		// save only files with txt ending
		for (int i = 0; i < entries.length; i++) {
			File toSave = new File(sourcePath, entries[i]);
			if (!toSave.isFile())
				continue;
			// be patient, this overwrites existing files
			if (FileOperations.getFileExtension(toSave.getName()).equals("txt")) {
				FileOperations.copyFileUsingStream(toSave, new File(destPath, toSave.getName()), false);
			}
		}
	}

	private static boolean profileExists(String profileName) {
		File profilePath = new File(profileFolderPath);
		if (!profilePath.exists() || !profilePath.isDirectory())
			return false;

		String[] entries = profilePath.list();
		for (String entry : entries) {
			File dir = new File(profilePath, entry);
			if (entry.equals(profileName) && dir.isDirectory())
				return true;

		}
		return false;
	}

	public static String[] getProfiles() {
		List<String> profilesList = new ArrayList<String>();
		File profilePath = new File(profileFolderPath);
		if (!profilePath.exists() || !profilePath.isDirectory())
			return new String[] {};

		String[] entries = profilePath.list();
		for (String entry : entries) {
			File dir = new File(profilePath, entry);
			if (dir.isDirectory())
				profilesList.add(entry);
		}
		return profilesList.toArray(new String[profilesList.size()]);
	}

}
