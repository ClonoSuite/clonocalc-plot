package util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import checkboxtree.CheckTreeManager;

public class TreeFuncs {

	public static List<String> getListOfCheckedFiles(CheckTreeManager checkTreeManager) {
		// to get the paths that were checked
		TreePath checkedPaths[] = checkTreeManager.getSelectionModel().getSelectionPaths();

		List<String> listCheckedFiles = new ArrayList<String>();

		for (TreePath path : checkedPaths) {
			listCheckedFiles.addAll(getListOfFilesFromPath(path));
		}

		return listCheckedFiles;
	}

	public static List<String> getListOfFilesFromPath(TreePath path) {
		List<String> listFiles = new ArrayList<String>();

		String fullPath = path.toString();
		fullPath = fullPath.substring(1, fullPath.length() - 1);
		String[] steps = fullPath.split(", ");
		if (steps.length == 3) {
			listFiles.add(steps[1] + File.separator + steps[2]);
		} else {
			TreeNode nodeToPath = (TreeNode) path.getLastPathComponent();
			for (int i = 0; i < nodeToPath.getChildCount(); i++) {
				TreePath childPath = getPath(nodeToPath.getChildAt(i));
				listFiles.addAll(getListOfFilesFromPath(childPath));
			}
		}
		return listFiles;
	}

	public static String treePathToString(TreePath path) {
		String fullPath = path.toString();
		fullPath = fullPath.substring(1, fullPath.length() - 1);
		String[] steps = fullPath.split(", ");
		String pathStr = "";
		for (int i = 1; i < steps.length; i++) {
			pathStr += steps[i] + File.separator;
		}
		return pathStr.substring(0, pathStr.length() - 1);
	}

	public static List<TreePath> getListOfLeafsFromPath(TreePath path) {
		List<TreePath> leafs = new ArrayList<TreePath>();
		TreeNode node = (TreeNode) path.getLastPathComponent();
		if (node.isLeaf()) {
			leafs.add(path);
			return leafs;
		}
		for (int i = 0; i < node.getChildCount(); i++) {
			TreeNode child = node.getChildAt(i);
			TreePath childPath = getPath(child);
			if (child.isLeaf()) {
				leafs.add(childPath);
			} else {
				leafs.addAll(getListOfLeafsFromPath(childPath));
			}
		}
		return leafs;
	}

	public static List<TreePath> expandCheckedPaths(TreePath[] checkedPaths) {
		List<TreePath> expandedCheckedPaths = new ArrayList<TreePath>();

		for (TreePath path : checkedPaths) {
			String fullPath = path.toString();
			fullPath = fullPath.substring(1, fullPath.length() - 1);
			String[] steps = fullPath.split(", ");
			if (steps.length == 3) {
				expandedCheckedPaths.add(path);
			} else {
				TreeNode nodeToPath = (TreeNode) path.getLastPathComponent();
				for (int i = 0; i < nodeToPath.getChildCount(); i++) {
					expandedCheckedPaths
							.addAll(expandCheckedPaths(new TreePath[] { getPath(nodeToPath.getChildAt(i)) }));
				}
			}
		}
		return expandedCheckedPaths;
	}

	public static List<TreePath> expandCheckedPathsArbitary(TreePath[] checkedPaths) {
		List<TreePath> expandedCheckedPaths = new ArrayList<TreePath>();

		for (TreePath path : checkedPaths) {
			TreeNode nodeToPath = (TreeNode) path.getLastPathComponent();
			// check if node is a child
			if (nodeToPath.isLeaf()) {

				expandedCheckedPaths.add(path);
				continue;
			}

			for (int i = 0; i < nodeToPath.getChildCount(); i++) {
				TreeNode childNode = (TreeNode) nodeToPath.getChildAt(i);
				// if node is not child, expand it
				expandedCheckedPaths.addAll(expandCheckedPathsArbitary(new TreePath[] { getPath(childNode) }));
			}

		}

		// for (TreePath path : checkedPaths) {
		// String fullPath = path.toString();
		// fullPath = fullPath.substring(1, fullPath.length() - 1);
		// String[] steps = fullPath.split(", ");
		// if (steps.length == 3) {
		// expandedCheckedPaths.add(path);
		// } else {
		// TreeNode nodeToPath = (TreeNode) path.getLastPathComponent();
		// for (int i = 0; i < nodeToPath.getChildCount(); i++) {
		// expandedCheckedPaths
		// .addAll(expandCheckedPaths(new TreePath[] { getPath(nodeToPath
		// .getChildAt(i)) }));
		// }
		// }
		// }
		return expandedCheckedPaths;
	}

	public static TreePath getPath(TreeNode treeNode) {
		List<Object> nodes = new ArrayList<Object>();
		if (treeNode != null) {
			nodes.add(treeNode);
			treeNode = treeNode.getParent();
			while (treeNode != null) {
				nodes.add(0, treeNode);
				treeNode = treeNode.getParent();
			}
		}

		return nodes.isEmpty() ? null : new TreePath(nodes.toArray());
	}

	public static void expandJTree(JTree tree) {
		int row = 0;
		while (row < tree.getRowCount()) {
			tree.expandRow(row);
			row++;
		}
	}

	public static String getFilePathFromTreePathArbitrary(TreePath path) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
		if (!node.isLeaf())
			return null;

		String fullPath = path.toString();
		fullPath = fullPath.substring(1, fullPath.length() - 1);
		String[] steps = fullPath.split(", ");
		String retpath = "";
		for (int i = 1; i < steps.length; i++)
			retpath += steps[i] + File.separator;
		// cut the last separator
		return retpath.substring(0, retpath.length() - 1);

	}

	public static TreePath getTreePathFromPath(DefaultMutableTreeNode parentNode, String restPath) {

		// DefaultMutableTreeNode fileNode;
		int nextSeparatorPos = restPath.indexOf(File.separator);
		String nextChildNodeName = "";
		if (nextSeparatorPos > 0) {
			nextChildNodeName = restPath.substring(0, nextSeparatorPos);
			restPath = restPath.substring(nextSeparatorPos + 1);

			for (int i = 0; i < parentNode.getChildCount(); i++) {
				DefaultMutableTreeNode nextChildNode = (DefaultMutableTreeNode) parentNode.getChildAt(i);
				if (nextChildNode.toString().equals(nextChildNodeName)) {
					return getTreePathFromPath(nextChildNode, restPath);
				}
			}
		} else {
			// check if file node already exists
			for (int i = 0; i < parentNode.getChildCount(); i++) {
				DefaultMutableTreeNode nextChildNode = (DefaultMutableTreeNode) parentNode.getChildAt(i);
				if (nextChildNode.toString().equals(restPath))
					return TreeFuncs.getPath(nextChildNode);
			}

		}
		return null;
	}

	public static TreePath addParameterToScript(DefaultMutableTreeNode rootNode, String file, String parameter) {
		DefaultMutableTreeNode functionNode = null;
		// search in all folder, normally there is only one called scripts
		for (int i = 0; i < rootNode.getChildCount(); i++) {
			DefaultMutableTreeNode folderNode = (DefaultMutableTreeNode) rootNode.getChildAt(i);
			// search in all folder files
			for (int b = 0; b < folderNode.getChildCount(); b++) {

				if (folderNode.getChildAt(b).toString().equals(file)) {
					functionNode = (DefaultMutableTreeNode) folderNode.getChildAt(b);

					// parameter already exists
					// for (int a = 0; a < functionNode.getChildCount(); a++) {
					// if (functionNode.getChildAt(a).toString()
					// .equals(parameter))
					// try {
					// throw new Exception("parameter " + parameter
					// + " alreasy exists");
					// } catch (Exception e) {
					// log.error(e, e);
					// }
					// }

					// DefaultMutableTreeNode parameterNode = new
					// DefaultMutableTreeNode(
					// parameter);
					// scriptTreeModel.insertNodeInto(parameterNode,
					// functionNode,
					// functionNode.getChildCount());
					return TreeFuncs.getPath(functionNode);
				}

			}
		}
		return null;
	}

	public static boolean isValidString(String[] allowedStrArr, String testStr) {
		for (String allowedStr : allowedStrArr)
			if (allowedStr.equals(testStr))
				return true;
		return false;
	}

	public static boolean checkForNodeName(TreeNode node, String nodeName) {
		if (node.toString().equals(nodeName))
			return true;

		for (int i = 0; i < node.getChildCount(); i++) {
			TreeNode child = node.getChildAt(i);
			if (checkForNodeName(child, nodeName))
				return true;
		}
		return false;
	}
}
