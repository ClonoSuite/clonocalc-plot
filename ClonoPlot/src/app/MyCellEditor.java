package app;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import util.ScriptParser;

public class MyCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 1L;
	private static final Border invalidBorder = new LineBorder(GlobalVars.colorInvalid);
	private static final Border validBorder = new LineBorder(GlobalVars.colorValid);
	private JTextField textField;

	public MyCellEditor(JTextField textField) {
		super(textField);
		this.textField = textField;
	}

	@Override
	public boolean stopCellEditing() {
		String validationErrorMessage = ScriptParser.validateValue(GlobalVars.singleton().getSelectedParameter(),
				textField.getText());
		// no error message
		if (validationErrorMessage == null)
			return super.stopCellEditing();

		// show pop with a hint
		JPopupMenu popup = new JPopupMenu();
		JMenuItem mi = new JMenuItem(validationErrorMessage);
		popup.add(mi);
		textField.add(popup);
		textField.setComponentPopupMenu(popup);
		popup.show(textField, -30, -30);

		textField.setBorder(invalidBorder);
		return false;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		textField.setBorder(validBorder);
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}
}
