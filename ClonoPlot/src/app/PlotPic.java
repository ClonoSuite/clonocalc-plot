package app;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;

public class PlotPic extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5880276774571241501L;
	Image img;

	public PlotPic(Image img) {
		this.img = img;
		MediaTracker mediaTracker = new MediaTracker(this);
		mediaTracker.addImage(img, 0);
		try {
			mediaTracker.waitForID(0);
		} catch (InterruptedException ie) {
			System.err.println(ie);
			System.exit(1);
		}
		setSize(img.getWidth(null), img.getHeight(null));
	}

	public void paint(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}
}
