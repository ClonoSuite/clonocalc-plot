package app;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Logger;

import util.OSDetection.OperatingSys;
import util.TimeLogger;

public class BashExecution {
	private static final Logger lol = Logger.getLogger(BashExecution.class);

	private Process proc = null;

	// final String scriptFileName = "rexec.sh";

	public void executeScript() {
		final GlobalVars gv = GlobalVars.singleton();
		final ActionListener pipelineFinishedListener = gv.PipelineFinshedListener;
		// if proc is running
		if (proc != null && isRunning(proc))
			return;

		try {
			String pathToRscript = System.getProperty("user.dir") + File.separator + gv.tmpFolder + File.separator
					+ gv.rscriptFileName;
			ProcessBuilder builder;
			if (GlobalVars.os == OperatingSys.Windows) {
				builder = new ProcessBuilder("Rscript", pathToRscript);
				Map<String, String> envs = builder.environment();
				envs.put("Path", envs.get("Path"));
				System.out.println(envs.get("Path"));
			} else {
				builder = new ProcessBuilder("Rscript", pathToRscript);
			}
			builder.redirectErrorStream(true);
			proc = builder.start();

			final Process runningProc = proc;
			final JScrollPane spTxtpDebug = GlobalVars.singleton().spTxtpDebug;

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						// measure runtime
						TimeLogger timelog = new TimeLogger(new String[] { "Runtime: " });

						InputStream stdout = runningProc.getInputStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));

						List<String> failedScriptsList = new ArrayList<String>();
						int scriptDoneCount = 0, scriptFailedCount = 0;
						String line = null, lineold = "Start execution";
						while ((line = reader.readLine()) != null) {
							lol.debug("Main: " + line);
							Color color = null;
							int fontSize = 0;
							if (line.contains(GlobalVars.doneScriptStr))
								scriptDoneCount++;
							else if (line.contains(GlobalVars.failedScriptStr)) {
								fontSize = 16;
								color = GlobalVars.colorInvalidFont;
								int scriptNumber = scriptDoneCount + scriptFailedCount;
								failedScriptsList
										.add(gv.getSelectedScriptRFuncNames().get(scriptDoneCount + scriptFailedCount)
												+ " failed! (Nr." + (scriptNumber + 1) + ")");
								scriptFailedCount++;
							}

							// mark warnings
							boolean warning = false;
							for (String warningStr : gv.warningStrArr)
								if (lineold.contains(warningStr))
									warning = true;
							if (warning) {
								color = GlobalVars.colorWarning;
								fontSize = 13;
							}

							append(spTxtpDebug, lineold + "\n", color, fontSize);
							lineold = line;
						}

						if (lineold.contains(gv.executionFinishedStr)) {
							if (scriptFailedCount > 0) {
								append(spTxtpDebug, scriptFailedCount + "/" + (scriptDoneCount + scriptFailedCount)
										+ " script(s) failed!\n", GlobalVars.colorInvalidFont);
								for (String fail : failedScriptsList) {
									append(spTxtpDebug, fail + "\n", GlobalVars.colorInvalidFont);
								}
							} else
								append(spTxtpDebug, "No errors detected.\n", GlobalVars.colorValidFont);
							reader.close();
							stdout.close();

							timelog.measureTime();
							timelog.measureTime();
							append(spTxtpDebug, timelog.getReport(), GlobalVars.colorValidFont);

						} else {
							lol.debug("Main: No finished signal.");
						}
						lol.debug("Execution thread finished.");
						reader.close();
						stdout.close();

						pipelineFinishedListener.actionPerformed(null);
					} catch (IOException e) {
						append(GlobalVars.singleton().spTxtpDebug, e.getMessage(), GlobalVars.colorInvalidFont);
						pipelineFinishedListener.actionPerformed(null);
						lol.error(e, e);
					}
				}
			}).start();

		} catch (IOException e) {
			append(GlobalVars.singleton().spTxtpDebug, e.getMessage(), GlobalVars.colorInvalidFont);
			pipelineFinishedListener.actionPerformed(null);
			lol.error(e, e);
		}
	}

	public void stopScript() {
		// if there any process to stop?
		if (proc == null)
			return;

		lol.debug("try to stop script");

		// kill running script
		// Runtime r = Runtime.getRuntime();
		// try {
		// //r.exec(new String[] { "bash", "-c", "pkill -9 -f 'bash -u" +
		// scriptFileName + "'" });
		// } catch (IOException e) {
		// log.error(e, e);
		// }
	}

	boolean isRunning(Process process) {
		try {
			process.exitValue();
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	public static void append(final JScrollPane spTxtpDebug, final String str, final Color color) {
		append(spTxtpDebug, str, color, 16);
	}

	public static void append(final JScrollPane spTxtpDebug, final String str, final Color color, final int size) {

		JViewport viewport = spTxtpDebug.getViewport();
		final JTextPane txtp = (JTextPane) viewport.getView();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Document doc = txtp.getDocument();
				StyledDocument doc = txtp.getStyledDocument();
				Style style = null;
				if (color != null) {
					style = txtp.addStyle("I'm a Style", null);
					StyleConstants.setForeground(style, color);
					if (size == 0)
						StyleConstants.setFontSize(style, 16);
					else
						StyleConstants.setFontSize(style, size);
				}

				if (doc != null) {
					try {
						// doc.insertString(doc.getLength(), str, null);
						doc.insertString(doc.getLength(), str, style);

						// int minimumValue =
						// spTxtpDebug.getVerticalScrollBar().getValue()
						// +
						// spTxtpDebug.getVerticalScrollBar().getVisibleAmount();
						// int maximumValue =
						// spTxtpDebug.getVerticalScrollBar().getMaximum();
						// if (maximumValue == minimumValue)
						spTxtpDebug.getVerticalScrollBar().setValue(spTxtpDebug.getVerticalScrollBar().getMaximum());

					} catch (BadLocationException e) {
					}
				}
			}
		});

	}

	public static void scrollToEnd(final JScrollPane spTxtpDebug) {

		int minimumValue = spTxtpDebug.getVerticalScrollBar().getValue()
				+ spTxtpDebug.getVerticalScrollBar().getVisibleAmount();
		int maximumValue = spTxtpDebug.getVerticalScrollBar().getMaximum();
		if (maximumValue == minimumValue)
			spTxtpDebug.getVerticalScrollBar().setValue(spTxtpDebug.getVerticalScrollBar().getMaximum());
	}

	public String getPATH() {
		try {
			GlobalVars gv = GlobalVars.singleton();
			ProcessBuilder builder = new ProcessBuilder("bash",
					System.getProperty("user.dir") + File.separator + gv.tmpFolder + File.separator + "test.sh");

			builder.redirectErrorStream(true);
			proc = builder.start();

			final Process runningProc = proc;

			InputStream stdout = runningProc.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));

			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			reader.close();
			stdout.close();
			return line;
		} catch (IOException e) {
			lol.error(e, e);
		}
		return "";
	}
}
