package app;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JTextArea;

import org.apache.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import util.CreateScript;

/// https://github.com/SurajGupta/RserveCLI2/issues/10
/// Extension methods for Rserve components
public class RserveExtensions {
	private static final Logger log = Logger.getLogger(CreateScript.class);
	private Frame f = new Frame("Error");
	private JTextArea jTextArea = new JTextArea();
	private int errorCounter = 0;
	private Point savedLocation;

	public RserveExtensions() {
		f.setEnabled(true);
		f.add(jTextArea);
	}

	// / Eval method that catches errors and returns the actual R error.
	// / See http://www.rforge.net/Rserve/faq.html#errors
	// / According to the Rserve
	// / docs the try() method is more reliable; however this method
	// / sometimes gives us a stack trace which is very helpful.
	public REXP TryEval(RConnection conn, String expr) {
		try {
			return conn.eval(expr);
		} catch (RserveException ex) {
			GetAndThrowRealError(conn, ex);
			return null; // We won't get here but the compiler doesn't know
							// that.
		}
	}

	// / VoidEval method with error reporting.
	public String TryVoidEval(RConnection conn, String expr) {
		try {
			conn.voidEval(expr);
		} catch (RserveException ex) {
			return GetAndThrowRealError(conn, ex);
		}
		return null;
	}

	// / Try to get a real error message and stack trace; if that fails rethrow
	// the original exception.
	private String GetAndThrowRealError(RConnection conn, RserveException ex) {

		String msg = "";
		String traceback = "";
		try {
			// Try to get the error message
			msg = conn.eval("geterrmessage()").asString();
		} catch (RserveException | REXPMismatchException e1) {
			log.error(e1, e1);
		}

		if (msg == null || msg.isEmpty())
			log.error("msg is null or empty!");

		// Try to get the stack trace
		// It's possible that geterrmessage() succeeds and traceback() fails.
		// If so just use the error message
		try {
			RList tracebacksList = conn.eval("traceback()").asList();
			for (Object obj : tracebacksList) {
				REXP rexp = (REXP) obj;
				traceback += rexp.asString() + "\r\n";
			}
			log.error("Rserve error: " + msg + "Traceback: " + traceback);
		} catch (RserveException | REXPMismatchException e) {
			log.error(e, e);
		}

		if (!f.isEnabled()) {
			f = new Frame("Error");
			f.add(jTextArea);
			f.setLocation(savedLocation);
		}
		jTextArea.setText("error " + errorCounter + ":\n" + msg + "\r\n"
				+ traceback + "\n" + jTextArea.getText());
		errorCounter++;
		f.addWindowListener(new WindowAdapter() { // just so we can
													// close
													// the window
			public void windowClosing(WindowEvent e) {
				savedLocation = f.getLocation();
				errorCounter = 0;
				jTextArea.setText("");
				f.setEnabled(false);
				f.dispose();
			}
		});
		f.pack();
		f.setVisible(true);
		return msg + "\r\n" + traceback;
	}
}
