package app;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import Listener.MyCellEditorListener;
import Listener.MyScriptParameterListSelectionListener;
import Listener.MyTreeExpansionListener;
import Listener.MyTreeSelectionListener;
import Listener.StartButtonListener;
import checkboxtree.CheckTreeManager;
import javaFX.SynchronousJFXDirChooser;
import net.miginfocom.swing.MigLayout;
import util.CreateScript;
import util.FileOperations;
import util.Profile;
import util.ScriptParser.Parameter;
import util.ScriptParser.Script;
import util.TreeFuncs;
import util.TreeRegex;
import util.TreeRegex.RegexNodeSelectionMode;

public class PanelConfig {
	private static final Logger log = Logger.getLogger(PanelConfig.class);

	MainFrame mainFrame = null;
	JFrame outerFrame = null;

	GlobalVars gv = GlobalVars.singleton();

	JPanel panelConfig = new JPanel();
	// JPanel panelOutputLine = new JPanel();
	JPanel panelAdvancedOptions = new JPanel();

	final String fileTypeReadUnpacked = "fastq";
	final String fileTypeReadPacked = "gz";

	// JLabel lblAddFolder = new JLabel("Add Folder");
	JLabel lblCountTablesText = new JLabel("Selected tables: ");
	JLabel lblCountTables = new JLabel("0");
	JLabel lblCountScriptText = new JLabel("Selected scripts: ");
	JLabel lblCountScript = new JLabel("0");

	JLabel lblInfoField = new JLabel("Info");
	JLabel lblOutputFolder = new JLabel("Output folder");

	JButton btnAddNewFolder = new JButton("Add new folder");

	JButton btnChooseOutputFolder = new JButton("Browse");

	JButton btnSaveProfile = new JButton("Save profile");
	JButton btnLoadProfile = new JButton("Load profile");
	JComboBox<String> combProfileName = new JComboBox<String>();

	JSeparator separatortop = new JSeparator();
	JSeparator separatortop2 = new JSeparator();
	JSeparator separatortop3 = new JSeparator();

	JSeparator separatorSample = new JSeparator();

	JButton btnRestoreDefaults = new JButton("Restore defaults");

	JMultilineLabel taScriptDescription = new JMultilineLabel("");

	public JTree tableTree = new JTree();
	CheckTreeManager tableTreeCheckManager;
	JScrollPane scrollpaneTableTree = new JScrollPane();

	public JTree scriptTree = new JTree();
	CheckTreeManager scriptTreeCheckManager;
	JScrollPane scrollpaneScriptTree = new JScrollPane();

	JTable namesTable = new JTableCustom();
	JScrollPane scrollnamesTable;

	JTableCustomComboBox pararTable = new JTableCustomComboBox();
	JScrollPane scrollparaTable;

	JButton btnNameUp = new JButton("\u2191");
	JButton btnNameDown = new JButton("\u2193");

	final String btnPreviewText = "Show preview";
	JButton btnPreview = new JButton(btnPreviewText);

	JButton btnStartPipeline = new JButton("Start");

	JComboBox<String> combSpeciesAlphabet = new JComboBox<String>();

	static Semaphore semaphore = new Semaphore(1);

	TreeRegex treeRegex = new TreeRegex();

	// Advanced options section
	JCheckBox cbAdvancedOptions = new JCheckBox("Advanced options");
	JTextField txtRegexPattern = new JTextField();
	JButton btnRegexAddDelete = new JButton("Search regex");
	JButton btnRestoreLastChanges = new JButton("Undo last Regex");
	JTable advancedOptTable = new JTableCustom();
	JScrollPane scrollAdvancedOptTable;
	JComboBox<RegexNodeSelectionMode> combRegexSelctionMode = new JComboBox<RegexNodeSelectionMode>();

	public JPanel getPanel() {
		return panelConfig;
	}

	public PanelConfig(MainFrame mainFrame, JFrame outerFrame) {
		this.mainFrame = mainFrame;
		this.outerFrame = outerFrame;

		// actionListener are added only one time
		initComponents();
		// paint components
		// repaintConfigScreen();
	}

	private void initComponents() {
		// deserialize objects
		// tableTree = gv.ser.loadTableJTree();
		// scriptTree = gv.ser.loadScriptJTree();

		// makes your tree as CheckTree
		tableTreeCheckManager = new CheckTreeManager(tableTree, false);
		scriptTreeCheckManager = new CheckTreeManager(scriptTree, false);

		btnAddNewFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// File file = chooseFile("Select a folder", filter, true);
				File file = SynchronousJFXDirChooser.chooseFileFX("Select a folder", null, true);
				if (file == null || !file.isDirectory() || file.getName().equals(""))
					return;

				addFilesToTree(tableTree, file);
			}
		});

		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveProfile("Backup_before_restore_defaults", false);
				restoreDefaults();
			}
		});

		btnChooseOutputFolder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// File file = chooseFile("Choose output folder", null, true);
				File file = SynchronousJFXDirChooser.chooseFileFX("Choose output folder", null, true);

				if (file == null || file.getAbsolutePath().equals("")) {
					gv.txtOutputFolder.setText(gv.defaultTxtOutputFolder);
					gv.txtOutputFolder.setBackground(GlobalVars.colorDefault);
				} else if (file.exists() && file.isDirectory()) {
					gv.txtOutputFolder.setText(file.getAbsolutePath());
					gv.txtOutputFolder.setBackground(GlobalVars.colorValid);
				} else {
					gv.txtOutputFolder.setBackground(GlobalVars.colorInvalid);
				}
			}
		});

		// one for the model and for the GUI changes
		btnStartPipeline.addActionListener(new StartButtonListener());
		btnStartPipeline.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				log.debug("Print checked Files:");
				List<String> listCheckedFiles = TreeFuncs.getListOfCheckedFiles(tableTreeCheckManager);
				for (String file : listCheckedFiles) {
					log.debug(file);
				}
				saveSelected(false);

				// reset Debug output Area
				GlobalVars.singleton().txtpDebug.setText("");
				mainFrame.switchToLoadingScreen();

				// create R script
				new CreateScript().createSkript();

				// start execution
				gv.bashExecution.executeScript();
			}
		});

		btnNameUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Size:" + scrollparaTable.getPreferredSize());
				int[] row_indexes = namesTable.getSelectedRows();

				if (row_indexes.length > 0) {
					int movedRow = row_indexes[0];
					if (movedRow > 0) {
						GlobalVars.singleton().namesTableGetUp(movedRow);
						// move selection interval to new position
						int newPos = movedRow - 1;
						namesTable.setRowSelectionInterval(newPos, newPos);
					}
				}
			}
		});
		btnNameDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int[] row_indexes = namesTable.getSelectedRows();

				if (row_indexes.length > 0) {
					int movedRow = row_indexes[0];

					if (movedRow < namesTable.getRowCount() - 1) {
						GlobalVars.singleton().namesTableGetDown(movedRow);
						// move selection interval to new position
						int newPos = movedRow + 1;
						namesTable.setRowSelectionInterval(newPos, newPos);
					}
				}
			}
		});

		// wait for the initialization
		btnPreview.setEnabled(false);
		javax.swing.Timer t = new javax.swing.Timer(500, new ActionListener() {
			int counter = 0;

			@Override
			public void actionPerformed(ActionEvent e) {
				counter++;
				String btnText = "Initialize R ";
				for (int i = 0; i < (counter % 4); i++)
					btnText += ".";
				for (int i = 0; i < 3 - (counter % 4); i++)
					btnText += " ";
				btnPreview.setText(btnText);
			}
		});
		t.start();
		// the action listener is called if R is ready
		ActionListener btnPreviewActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnPreview.setEnabled(true);
				btnPreview.setText(btnPreviewText);
				t.stop();
			}
		};
		btnPreview.addActionListener(btnPreviewActionListener);
		gv.setBtnPreviewActionListener(btnPreviewActionListener);
		btnPreview.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Script selectedScript = gv.getSelectedScript();

				if (selectedScript == null)
					return;

				new Thread(new Runnable() {
					@Override
					public void run() {

						try {
							semaphore.acquire();
						} catch (InterruptedException e1) {
							log.error("no semaphore availble");
						}

						javax.swing.Timer t = new javax.swing.Timer(500, new ActionListener() {
							int counter = 0;

							@Override
							public void actionPerformed(ActionEvent e) {
								counter++;
								String btnText = btnPreviewText + " ";
								for (int i = 0; i < (counter % 4); i++)
									btnText += ".";
								for (int i = 0; i < 3 - (counter % 4); i++)
									btnText += " ";
								btnPreview.setText(btnText);
							}
						});
						t.start();

						if (gv.isNamesTableChanged()) {
							log.info("reaload R tables");
							gv.setNamesTableChanged(false);
							// gv.plotDemo.initLibrarysAndFuncs();
							gv.plotDemo.updateDataTables();
						}
						gv.plotDemo.plotScriptPDFBoxPerformance(selectedScript);

						semaphore.release();
						t.stop();
						btnPreview.setText(btnPreviewText);
					}
				}).start();

			}
		});

		btnSaveProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String profileName = (String) combProfileName.getSelectedItem();
				saveProfile(profileName, true);
			}
		});
		btnLoadProfile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Profile.loadProfile((String) combProfileName.getSelectedItem())) {
					gv.reloadProfile();
					reloadComponents();
					combProfileName.setModel(new DefaultComboBoxModel<String>(Profile.getProfiles()));
				}
			}
		});

		combProfileName.setEditable(true);
		combProfileName.setModel(new DefaultComboBoxModel<String>(Profile.getProfiles()));
		combProfileName.setPrototypeDisplayValue("I_am_a_big_long");

		String[] possibleSpeciesAlphabets = FileOperations
				.readFile(GlobalVars.mainScriptsDirAbsolutePath + File.separator + GlobalVars.speciesAlphabetPath)
				.split("\n");
		combSpeciesAlphabet.setModel(new DefaultComboBoxModel<String>(possibleSpeciesAlphabets));

		// set initial value
		gv.setUsedSpeciesAlphabet((String) combSpeciesAlphabet.getSelectedItem());
		combSpeciesAlphabet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gv.setUsedSpeciesAlphabet((String) combSpeciesAlphabet.getSelectedItem());
				// change alphabet in R session
				gv.setNamesTableChanged(true);
			}
		});

		txtRegexPattern.setText("Ich bin ein langer Beispieltext");
		txtRegexPattern.setMaximumSize(txtRegexPattern.getPreferredSize());
		txtRegexPattern.setText("");

		tableTree.setModel(gv.tableTreeModel);
		tableTree.addTreeExpansionListener(new MyTreeExpansionListener(this));
		tableTree.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent event) {
				if (((event.getModifiers() & InputEvent.BUTTON3_MASK) != 0) && tableTreeSelectedNodeIsDeletable()) {
					showMenu(event.getX(), event.getY());
				}
			}

			protected void showMenu(int x, int y) {
				JPopupMenu popup = new JPopupMenu();
				JMenuItem mi = new JMenuItem("Delete");
				popup.add(mi);
				mi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						tableTreeDeleteSelectedItems();
					}
				});
				popup.show(tableTree, x, y);
			}
		});
		tableTree.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					if (tableTreeSelectedNodeIsDeletable())
						tableTreeDeleteSelectedItems();
				}
			}
		});
		scrollpaneTableTree.getViewport().add(tableTree);

		scriptTree.setModel(gv.scriptTreeModel);
		scriptTree.addTreeSelectionListener(new MyTreeSelectionListener(this));
		scrollpaneScriptTree.getViewport().add(scriptTree);

		scriptTreeCheckManager.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {

			@Override
			public void valueChanged(TreeSelectionEvent e) {
				// update script selection counters
				List<TreePath> list = TreeFuncs
						.expandCheckedPathsArbitary(scriptTreeCheckManager.getSelectionModel().getSelectionPaths());
				int selectedScriptsCount = list.size();
				lblCountScript.setText(Integer.toString(selectedScriptsCount));
				gv.selectedScriptsCount = selectedScriptsCount;
			}
		});

		namesTable.setDragEnabled(true);
		namesTable.setDropMode(DropMode.INSERT_ROWS);
		namesTable.setTransferHandler(new TransferHandlerTableRow(namesTable));

		namesTable.setModel(gv.namesTableModel);
		namesTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		namesTable.setShowGrid(false);
		namesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		namesTable.getColumnModel().getColumn(0).setCellRenderer(new LeftDotToolTipRenderer());
		namesTable.getModel().addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				// update table selection counter
				if (e.getType() == TableModelEvent.INSERT || e.getType() == TableModelEvent.DELETE) {
					lblCountTables
							.setText(Integer.toString(TreeFuncs.getListOfCheckedFiles(tableTreeCheckManager).size()));
				}
			}
		});
		namesTable.getDefaultEditor(String.class).addCellEditorListener(new MyCellEditorListener() {
			@Override
			public void editingStopped(ChangeEvent e) {
				if (namesTable.getSelectedRow() == -1)
					return;

				TreeCellEditor model = (TreeCellEditor) e.getSource();
				String newValue = (String) model.getCellEditorValue();
				log.debug(newValue);
				String pathStr = (String) namesTable.getValueAt(namesTable.getSelectedRow(), 0);
				gv.getHashmapTableName().get(pathStr).name = newValue;
			}
		});
		namesTable.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent event) {
				// Right mouse click
				if (SwingUtilities.isRightMouseButton(event)) {
					// get the coordinates of the mouse click
					Point p = event.getPoint();

					// get the row index that contains that coordinate
					int rowNumber = namesTable.rowAtPoint(p);

					// Get the ListSelectionModel of the JTable
					ListSelectionModel model = namesTable.getSelectionModel();

					// set the selected interval of rows. Using the "rowNumber"
					// variable for the beginning and end selects only that one
					// row.
					model.setSelectionInterval(rowNumber, rowNumber);
				}

				if (((event.getModifiers() & InputEvent.BUTTON3_MASK) != 0) && namesTable.getSelectedRow() != -1) {
					showMenu(event.getX(), event.getY());
				}
			}

			protected void showMenu(int x, int y) {
				JPopupMenu popup = new JPopupMenu();
				JMenuItem mi = new JMenuItem("Delete");
				popup.add(mi);
				mi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						namesTableDeleteSelectedItem();
					}
				});
				popup.show(namesTable, x, y);
			}
		});
		namesTable.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					namesTableDeleteSelectedItem();
				}
			}
		});

		pararTable.getSelectionModel().addListSelectionListener(new MyScriptParameterListSelectionListener(this));
		pararTable.setShowGrid(false);
		pararTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// for user input (parameter values) validation
		pararTable.setDefaultEditor(Object.class, new MyCellEditor(new JTextField()));

		scrollnamesTable = new JScrollPane(namesTable);
		scrollparaTable = new JScrollPane(pararTable);

		// Advanced options section
		for (RegexNodeSelectionMode regexNodeSelectionMode : RegexNodeSelectionMode.values())
			combRegexSelctionMode.addItem(regexNodeSelectionMode);

		btnRegexAddDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String ret = treeRegex.treeRegexNodeSelection(tableTree, tableTreeCheckManager,
						txtRegexPattern.getText(), (RegexNodeSelectionMode) combRegexSelctionMode.getSelectedItem(),
						gv);
				if (!ret.equals("")) {
					// error occured
					PlotDemo.changeTitleForSec(outerFrame, ret, 3);
				}
			}
		});

		btnRestoreLastChanges.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!treeRegex.restoreLastChanges(tableTreeCheckManager, gv)) {
					// error occured
					PlotDemo.changeTitleForSec(outerFrame, "Nothing to restore!", 3);
				}
			}
		});

		cbAdvancedOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gv.advancedOptions = cbAdvancedOptions.isSelected();
				repaintConfigScreen();
				outerFrame.validate();
			}
		});
		advancedOptTable.setModel(gv.advancedOptTableModel);
		advancedOptTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		advancedOptTable.getDefaultEditor(String.class).addCellEditorListener(new MyCellEditorListener() {
			@Override
			public void editingStopped(ChangeEvent e) {
				TreeCellEditor model = (TreeCellEditor) e.getSource();
				String newValue = (String) model.getCellEditorValue();
				log.debug(newValue);
				if (advancedOptTable.getSelectedRow() != -1) {
					String advOpt = (String) advancedOptTable.getValueAt(advancedOptTable.getSelectedRow(), 0);
					// sets a reload flag for the r data frames
					gv.criticalAdvOptHandling(advOpt);
				}
			}
		});

		scrollAdvancedOptTable = new JScrollPane(advancedOptTable);

		fillComponents();

		// auto wrap
		taScriptDescription.setLineWrap(true);
		// do not wrap in a wordremoveSelectionPaths
		taScriptDescription.setWrapStyleWord(true);
		taScriptDescription.setEditable(false);

		// resizeTableTree();
	}

	private void buildConfigScreen() {

		panelConfig.setLayout(new MigLayout());

		// LINE 1

		// panelConfig.add(separatortop, "growx, span");

		// LINE 2
		panelConfig.add(btnAddNewFolder, "split 6, span");
		panelConfig.add(lblInfoField);

		panelConfig.add(combProfileName, "growx, push, align right");
		panelConfig.add(btnSaveProfile, "align right");
		panelConfig.add(btnLoadProfile, "align right");

		panelConfig.add(btnRestoreDefaults, "align right, wrap");

		panelConfig.add(separatortop2, "growx, span");

		// LINE 3
		panelConfig.add(lblOutputFolder, "split 3, span");
		panelConfig.add(GlobalVars.singleton().txtOutputFolder, "growx, push");
		panelConfig.add(btnChooseOutputFolder, "wrap");

		panelConfig.add(separatortop3, "growx, span");

		// LINE 4
		panelConfig.add(lblCountTablesText, "split 2");
		panelConfig.add(lblCountTables);

		panelConfig.add(lblCountScriptText, "split 2");
		panelConfig.add(lblCountScript, "wrap");

		// LINE 5 table tree
		panelConfig.add(scrollpaneTableTree, "h 100%,grow, push");

		// script tree
		panelConfig.add(scrollpaneScriptTree, "h 100%,grow, push, wrap");

		// LINE 6

		panelConfig.add(scrollnamesTable, "h 65%,grow, push");

		panelConfig.add(scrollparaTable, "h 65%,grow, push, wrap");

		if (cbAdvancedOptions.isSelected()) {
			panelConfig.add(scrollAdvancedOptTable, "h 60%, growx");
			panelConfig.add(txtRegexPattern, "growx, split 4");
			panelConfig.add(combRegexSelctionMode);
			panelConfig.add(btnRestoreLastChanges);
			panelConfig.add(btnRegexAddDelete, "wrap");
		}

		// LINE 7
		JPanel jPanelBtnNameUpDown = new JPanel();
		jPanelBtnNameUpDown.add(btnNameUp, "align left");
		jPanelBtnNameUpDown.add(btnNameDown, "align right");
		jPanelBtnNameUpDown.add(cbAdvancedOptions, "align right");

		panelConfig.add(jPanelBtnNameUpDown, "split 2, w 20%");

		JPanel jPanelBtnStartPipeline = new JPanel();
		jPanelBtnStartPipeline.add(btnStartPipeline);
		jPanelBtnStartPipeline.add(btnPreview);
		jPanelBtnStartPipeline.add(combSpeciesAlphabet);
		panelConfig.add(jPanelBtnStartPipeline, "w 20%");

		panelConfig.add(taScriptDescription, "w 50%, growx, push, wmin 10");
	}

	public void repaintConfigScreen() {
		// first remove all components from the panel
		panelConfig.removeAll();
		// panelOutputLine.removeAll();
		// panelAdvancedOptions.removeAll();
		// add the components to the panel
		buildConfigScreen();
	}

	private void restoreDefaults() {
		// save selected script table
		TreePath selectedScriptTablePath = gv.getSelectedScriptTablePath();

		clearComponents();

		GlobalVars.singleton().restoreDefaults();
		TreeFuncs.expandJTree(scriptTree);

		// restore selected ScriptTable
		if (selectedScriptTablePath != null) {
			TreePath tablePath = gv.getHashmapStrToTreePath(selectedScriptTablePath.toString());
			changeScriptParameterTable(tablePath);
			scriptTree.getSelectionModel().setSelectionPath(tablePath);
		}

		outerFrame.validate();
		outerFrame.revalidate();
		// repaintConfigScreen();
		// SwingUtilities.invokeLater(new Runnable() {
		// public void run() {
		//
		// }
		// });
	}

	public void saveSelected(boolean exit) {
		// serialize trees
		// gv.ser.saveTableJTree(tableTree);
		// gv.ser.saveScriptJTree(scriptTree);

		// save Tree expansion state
		gv.ser.saveTableJTreeExpansionState(tableTree);
		gv.ser.saveScriptJTreeExpansionState(scriptTree);

		TreePath scriptCheckedPaths[] = scriptTreeCheckManager.getSelectionModel().getSelectionPaths();
		// TreePath tableCheckedPaths[] = tableTreeCheckManager
		// .getSelectionModel().getSelectionPaths();

		// gv.writeSelectedToFile(expandCheckedPaths(checkedPaths));
		// if (!exit || scriptCheckedPaths.length > 0) {
		List<TreePath> paths = TreeFuncs.expandCheckedPathsArbitary(scriptCheckedPaths);
		gv.writeSelectedToFile(paths);
		// }
	}

	private void saveProfile(String profileName, boolean askForOverwrite) {
		if (profileName.equals(""))
			return;
		// save current app state
		saveSelected(false);
		GlobalVars.singleton().onExit();
		// copy state to profile folder
		Profile.saveCurrentProfile(profileName, askForOverwrite);
		// update possible profiles
		combProfileName.setModel(new DefaultComboBoxModel<String>(Profile.getProfiles()));
	}

	private void fillComponents() {
		gv.restoreTableTree(tableTreeCheckManager, tableTree);
		gv.ser.restoreTableJTreeExpansionState(tableTree);

		gv.restoreScriptTree(scriptTreeCheckManager);
		gv.ser.restoreScriptJTreeExpansionState(scriptTree);

		// restore the table names after the default names are loaded
		gv.restoreNamesTable();
	}

	private void clearComponents() {
		DefaultTableModel dm = (DefaultTableModel) namesTable.getModel();
		for (int i = dm.getRowCount() - 1; i >= 0; i--) {
			dm.removeRow(i);
		}

		// avoid dead selections
		scriptTreeCheckManager.getSelectionModel().removeSelectionAll();
		tableTreeCheckManager.getSelectionModel().removeSelectionAll();

		DefaultTableModel tableModel = (DefaultTableModel) pararTable.getModel();
		tableModel.setRowCount(0);
	}

	public void reloadComponents() {
		clearComponents();
		fillComponents();
	}

	void addFilesToTree(JTree tree, File folder) {
		String curPath = folder.getPath();

		int value = gv.addFilesToTableTree(tree, folder, tableTreeCheckManager);

		if (value > 0) {
			lblInfoField.setText("Added " + value + " new files");
		} else if (value == 0) {
			lblInfoField.setText("Nothing new in " + curPath);
		} else /* if (value == -1) */ {
			lblInfoField.setText("No xls/csv file found in " + curPath);
		}

	}

	private void tableTreeDeleteSelectedItems() {
		TreePath[] paths = tableTree.getSelectionPaths();

		for (int i = 0; i < paths.length; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) (paths[i].getLastPathComponent());
			gv.delSampleFromTableAndTree(paths[i], tableTreeCheckManager);
			gv.tableTreeModel.removeNodeFromParent(node);
		}
	}

	private boolean tableTreeSelectedNodeIsDeletable() {
		// check if selected path is a leaf
		// by design it's only possible to delete complete folders in
		// the tree because files are parsed automatically by endings
		TreePath[] selectedPaths = tableTree.getSelectionPaths();
		if (selectedPaths == null)
			return false;
		TreeNode node = (DefaultMutableTreeNode) selectedPaths[0].getLastPathComponent();

		if ((tableTree.getSelectionCount() > 0) && !node.isLeaf() && node != tableTree.getModel().getRoot())
			return true;
		return false;
	}

	private void namesTableDeleteSelectedItem() {
		// no row selected
		if (namesTable.getSelectedRow() == -1)
			return;

		String pathStr = (String) namesTable.getValueAt(namesTable.getSelectedRow(), 0);
		// HashMap<String, Pair> bla = gv.getHashmapTableName();
		gv.delSampleFromTableAndTree(gv.getHashmapTableName().get(pathStr).getPath(), tableTreeCheckManager);
	}

	public void collapsePathInScriptTree(TreePath path) {
		scriptTree.collapsePath(path);
	}

	public void changeScriptParameterTable(TreePath path) {
		Script script = gv.getTableModelFromPath(path);
		if (script != null) {
			gv.setSelectedScript(script);
			DefaultTableModel model = script.getDefaultTableModel();
			pararTable.setModel(model);

			// pararTable.getColumnModel().getColumn(1).setPreferredWidth(300);
			// pararTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			// lblScriptDescriptionText.setText("Function description: ");
			// lblScriptDescription.setText(script.description);
			taScriptDescription.setText(script.getName() + ": " + script.getDescription().replace("\\n", "\n"));

			// refresh combo boxes
			pararTable.clearComboBoxesMap();

			// create combo boxes in Table
			for (int i = 0; i < model.getRowCount(); i++) {
				String parameterName = (String) model.getValueAt(i, 0);
				Parameter parameter = script.getMapParameter().get(parameterName);

				if (parameter.isBoolean()) {
					pararTable.addRowCombobox(i, new String[] { "FALSE", "TRUE" });
				}

				List<String> allowedValues = parameter.getAllowedValues();
				if (allowedValues.size() > 0) {
					pararTable.addRowCombobox(i, allowedValues);
				}
			}
		}
	}

	public void changeScriptParameterInATable() {
		Script script = gv.getSelectedScript();
		if (script != null) {
			// break if no row is selected
			if (pararTable.getSelectedRow() == -1)
				return;
			// get selected row and coresponding parameter name
			String parameterName = (String) pararTable.getValueAt(pararTable.getSelectedRow(), 0);
			// try to get Parameter instance
			Parameter parameter = script.getMapParameter().get(parameterName);
			if (parameter == null)
				return;

			gv.setSelectedParameter(parameter);

			// lblScriptDescriptionText.setText("Parameter description: ");
			// fill label with parameter description
			// lblScriptDescription.setText(parameter.description);
			// System.out.println(parameter.description);
			// lblScriptDescriptionText.setSize(lblScriptDescriptionText.getPreferredSize());
			// lblScriptDescription.setSize(lblScriptDescription.getPreferredSize());
			taScriptDescription.setText(parameterName + ": " + parameter.getDescription().replace("\\n", "\n"));
		}

	}

	public void changeBtnPreviewCol(boolean fast) {
		if (fast)
			btnPreview.setBackground(Color.GREEN);
		else
			btnPreview.setBackground(Color.RED);

	}
}
