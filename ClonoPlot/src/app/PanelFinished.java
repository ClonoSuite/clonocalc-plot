package app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import net.miginfocom.swing.MigLayout;
import util.OSDetection.OperatingSys;

public class PanelFinished implements ActionListener {
	private static final Logger log = Logger.getLogger(PanelFinished.class);

	MainFrame mainFrame = null;
	JFrame outerFrame = null;

	JPanel panelFinished = new JPanel();
	JPanel panelButtons = new JPanel();

	JScrollPane spTxtpDebug = GlobalVars.singleton().spTxtpDebug;

	JButton btnShowResultFolder = new JButton("Show result folder");
	JButton btnBackToStart = new JButton("Back to start");
	JButton btnBugReport = new JButton("Create bug report");

	public JPanel getPanel() {
		return panelFinished;
	}

	public PanelFinished(MainFrame mainFrame, JFrame outerFrame) {
		this.mainFrame = mainFrame;
		this.outerFrame = outerFrame;

		initComponents();
		// buildFinishedScreen();
	}

	private void initComponents() {
		btnShowResultFolder.addActionListener(this);
		btnBackToStart.addActionListener(this);
		btnBugReport.addActionListener(this);
	}

	private void buildFinishedScreen() {
		panelFinished.setLayout(new BorderLayout());
		// panelFinished.setLayout(new MigLayout());
		panelButtons.setLayout(new MigLayout());

		// spTxtpDebug.setSize(300, 300);
		panelFinished.add(spTxtpDebug, BorderLayout.CENTER);

		panelButtons.add(btnBackToStart);
		panelButtons.add(btnShowResultFolder);
		panelButtons.add(btnBugReport, "align right");

		panelFinished.add(panelButtons, BorderLayout.SOUTH);
		// panelFinished.add(panelButtons, "dock south, growx");
	}

	public void repaintFinishedScreen() {
		// first remove all components from the panel
		panelFinished.removeAll();
		// add the components to the panel
		buildFinishedScreen();

		// outerFrame.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnShowResultFolder) {
			OpenFolder(GlobalVars.singleton().txtOutputFolder.getText());
		} else if (e.getSource() == this.btnBackToStart) {
			mainFrame.switchToConfigScreen();
		} else if (e.getSource() == this.btnBugReport) {
			String bugReportZipAbsolutePath = GlobalVars.singleton().createBugReport();
			BashExecution.append(spTxtpDebug, "Bug report saved to " + bugReportZipAbsolutePath, Color.BLUE, 15);
		}
	}

	private void OpenFolder(String pathToFolder) {
		try {
			File dir = new File(pathToFolder);
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(dir);
			} else {

				ProcessBuilder builder;
				GlobalVars.singleton();
				if (GlobalVars.os == OperatingSys.Linux)
					builder = new ProcessBuilder("xdg-open", pathToFolder);
				else
					builder = new ProcessBuilder("open", pathToFolder);

				Process proc = builder.start();
				proc.waitFor();
			}
		} catch (IOException e) {
			log.error(e, e);
		} catch (InterruptedException e) {
			log.error(e, e);
		}
	}

}
