package javaFX;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

import app.GlobalVars;
import app.PanelConfig;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * A utility class that summons JavaFX DirectoryChooser from the Swing EDT. (Or
 * anywhere else for that matter.) JavaFX should be initialized prior to using
 * this class (e. g. by creating a JFXPanel instance). It is also recommended to
 * call Platform.setImplicitExit(false) after initialization to ensure that
 * JavaFX platform keeps running. Don't forget to call Platform.exit() when
 * shutting down the application, to ensure that the JavaFX threads don't
 * prevent JVM exit.
 */
public class SynchronousJFXDirChooser {
	private static final Logger log = Logger.getLogger(PanelConfig.class);

	private final Supplier<DirectoryChooser> DirectoryChooserFactory;

	/**
	 * Constructs a new file chooser that will use the provided factory.
	 * 
	 * The factory is accessed from the JavaFX event thread, so it should either
	 * be immutable or at least its state shouldn't be changed randomly while
	 * one of the dialog-showing method calls is in progress.
	 * 
	 * The factory should create and set up the chooser, for example, by setting
	 * extension filters. If there is no need to perform custom initialization
	 * of the chooser, DirectoryChooser::new could be passed as a factory.
	 * 
	 * Alternatively, the method parameter supplied to the showDialog() function
	 * can be used to provide custom initialization.
	 * 
	 * @param DirectoryChooserFactory
	 *            the function used to construct new choosers
	 */
	public SynchronousJFXDirChooser(Supplier<DirectoryChooser> DirectoryChooserFactory) {
		this.DirectoryChooserFactory = DirectoryChooserFactory;
	}

	/**
	 * Shows the DirectoryChooser dialog by calling the provided method.
	 * 
	 * Waits for one second for the dialog-showing task to start in the JavaFX
	 * event thread, then throws an IllegalStateException if it didn't start.
	 * 
	 * @see #showDialog(java.util.function.Function, long,
	 *      java.util.concurrent.TimeUnit)
	 * @param <T>
	 *            the return type of the method, usually File or
	 *            List&lt;File&gt;
	 * @param method
	 *            a function calling one of the dialog-showing methods
	 * @return whatever the method returns
	 */
	public <T> T showDialog(Function<DirectoryChooser, T> method) {
		return showDialog(method, 1, TimeUnit.SECONDS);
	}

	/**
	 * Shows the DirectoryChooser dialog by calling the provided method. The
	 * dialog is created by the factory supplied to the constructor, then it is
	 * shown by calling the provided method on it, then the result is returned.
	 * <p>
	 * Everything happens in the right threads thanks to
	 * {@link SynchronousJFXCaller}. The task performed in the JavaFX thread
	 * consists of two steps: construct a chooser using the provided factory and
	 * invoke the provided method on it. Any exception thrown during these steps
	 * will be rethrown in the calling thread, which shouldn't normally happen
	 * unless the factory throws an unchecked exception.
	 * </p>
	 * <p>
	 * If the calling thread is interrupted during either the wait for the task
	 * to start or for its result, then null is returned and the Thread
	 * interrupted status is set.
	 * </p>
	 * 
	 * @param <T>
	 *            return type (usually File or List&lt;File&gt;)
	 * @param method
	 *            a function that calls the desired DirectoryChooser method
	 * @param timeout
	 *            time to wait for Platform.runLater() to <em>start</em> the
	 *            dialog-showing task (once started, it is allowed to run as
	 *            long as needed)
	 * @param unit
	 *            the time unit of the timeout argument
	 * @return whatever the method returns
	 * @throws IllegalStateException
	 *             if Platform.runLater() fails to start the dialog-showing task
	 *             within the given timeout
	 */
	public <T> T showDialog(Function<DirectoryChooser, T> method, long timeout, TimeUnit unit) {
		Callable<T> task = () -> {
			DirectoryChooser chooser = DirectoryChooserFactory.get();
			return method.apply(chooser);
		};
		SynchronousJFXCaller<T> caller = new SynchronousJFXCaller<>(task);
		try {
			return caller.call(timeout, unit);
		} catch (RuntimeException | Error ex) {
			throw ex;
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
			return null;
		} catch (Exception ex) {
			throw new AssertionError("Got unexpected checked exception from" + " SynchronousJFXCaller.call()", ex);
		}
	}

	/**
	 * Shows a DirectoryChooser using DirectoryChooser.showOpenDialog().
	 * 
	 * @see #showDialog(java.util.function.Function, long,
	 *      java.util.concurrent.TimeUnit)
	 * @return the return value of DirectoryChooser.showOpenDialog()
	 */
	public File showDialog() {
		return showDialog(chooser -> chooser.showDialog(null));
	}

	public static File chooseFileFX(String title, FileFilter filter, boolean folder) {

		// used for initializing javafx thread (ideally called once)
		@SuppressWarnings("unused")
		JFXPanel dummy = new javafx.embed.swing.JFXPanel();
		Platform.setImplicitExit(false);

		try {
			if (folder) {
				SynchronousJFXDirChooser chooser = new SynchronousJFXDirChooser(() -> {
					DirectoryChooser ch = new DirectoryChooser();
					ch.setTitle(title);
					File lastSelectedFile = GlobalVars.singleton().getLastSelectedDir();
					if (lastSelectedFile != null && lastSelectedFile.exists() && lastSelectedFile.isDirectory())
						ch.setInitialDirectory(lastSelectedFile);
					return ch;
				});
				File choosenFolder = chooser.showDialog();
				if (choosenFolder != null)
					GlobalVars.singleton().setLastSelectedDir(new File(choosenFolder.getParent()));
				return choosenFolder;
			} else {
				SynchronousJFXFileChooser chooser = new SynchronousJFXFileChooser(() -> {
					FileChooser ch = new FileChooser();
					// TODO
					// ch.setSelectedExtensionFilter(filter);
					ch.setTitle(title);
					return ch;
				});
				return chooser.showOpenDialog();
			}
		} catch (Exception e) {
			log.error(e, e);
		}
		return null;
	}

	public static File saveFile(String title, Collection<ExtensionFilter> filterCollection) {

		// used for initializing javafx thread (ideally called once)
		@SuppressWarnings("unused")
		JFXPanel dummy = new javafx.embed.swing.JFXPanel();
		Platform.setImplicitExit(false);

		try {
			SynchronousJFXFileChooser chooser = new SynchronousJFXFileChooser(() -> {
				FileChooser ch = new FileChooser();
				ch.getExtensionFilters().addAll(filterCollection);
				ch.setTitle(title);

				File lastSavedFileFolder = GlobalVars.singleton().getLastSavedFileFolder();
				if (lastSavedFileFolder != null && lastSavedFileFolder.exists() && lastSavedFileFolder.isDirectory())
					ch.setInitialDirectory(lastSavedFileFolder);

				String lastSavedFileName = GlobalVars.singleton().getLastSavedFileName();
				if (lastSavedFileFolder != null)
					ch.setInitialFileName(lastSavedFileName);
				return ch;
			});
			File choosenFile = chooser.showSaveDialog();
			if (choosenFile != null) {
				GlobalVars.singleton().setLastSavedFileFolder(new File(choosenFile.getParent()));
				GlobalVars.singleton().setLastSavedFileName(choosenFile.getName());
			}
			return choosenFile;
		} catch (Exception e) {
			log.error(e, e);
		}
		return null;
	}
}