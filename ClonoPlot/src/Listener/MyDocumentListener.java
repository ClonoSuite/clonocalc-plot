package Listener;

import java.io.File;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;

public class MyDocumentListener implements DocumentListener {

	@Override
	public void changedUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		readFileNameValidation(e);
	}

	public void readFileNameValidation(DocumentEvent e) {
		JTextField owner = (JTextField) e.getDocument().getProperty("owner");

		GlobalVars gv = GlobalVars.singleton();

		if (owner == gv.txtOutputFolder) {
			File path = new File(gv.txtOutputFolder.getText());
			if (gv.txtOutputFolder.getText().equals("")) {
				gv.txtOutputFolder.setBackground(GlobalVars.colorDefault);
			} else if (path.exists() && path.isDirectory()) {
				gv.txtOutputFolder.setBackground(GlobalVars.colorValid);
			} else {
				gv.txtOutputFolder.setBackground(GlobalVars.colorInvalid);
			}
		}
	}

}
