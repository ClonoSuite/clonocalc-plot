package Listener;

import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.TreeCellEditor;

import org.apache.log4j.Logger;

public class MyCellEditorListener implements CellEditorListener {
	private static final Logger log = Logger.getLogger(MyCellEditorListener.class);

	@Override
	public void editingStopped(ChangeEvent e) {
		TreeCellEditor model = (TreeCellEditor) e.getSource();
		String newValue = (String) model.getCellEditorValue();
		log.debug(newValue);
		// TODO Auto-generated method stub

	}

	@Override
	public void editingCanceled(ChangeEvent e) {
		// TODO Auto-generated method stub

	}

}
