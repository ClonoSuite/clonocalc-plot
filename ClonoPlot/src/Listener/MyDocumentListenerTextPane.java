package Listener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.GlobalVars;

public class MyDocumentListenerTextPane implements DocumentListener {

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		GlobalVars gv = GlobalVars.singleton();
		int minimumValue = gv.spTxtpDebug.getVerticalScrollBar().getValue()
				+ gv.spTxtpDebug.getVerticalScrollBar().getVisibleAmount();
		int maximumValue = gv.spTxtpDebug.getVerticalScrollBar().getMaximum();
		if (maximumValue == minimumValue)
			gv.spTxtpDebug.getVerticalScrollBar().setValue(gv.spTxtpDebug.getVerticalScrollBar().getMaximum());
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub

	}

}
